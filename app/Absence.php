<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absence extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function player() {
    	return $this->belongsTo('App\Player');
    }

    public function game() {
    	return $this->belongsTo('App\Game');
    }

    public function substitution() {
    	return $this->belongsTo('App\Substitution');
    }
}
