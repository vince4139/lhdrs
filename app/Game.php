<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'schedule';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'publicity_home', 'publicity_away', 'home_room', 'away_room'
    ];

    protected $casts = [
        'date' => 'datetime:Y-m-d',
    ];

    public function league() {
    	return $this->belongsTo('App\League');
    }

    public function season() {
    	return $this->belongsTo('App\Season');
    }

    public function gamesheet() {
        return $this->hasOne('App\Gamesheet');
    }

    public function goals() {
        return $this->hasMany('App\Goal');
    }

    public function type() 
    {
        return $this->belongsTo('App\MatchType', 'match_type');
    }

    public function players() 
    {
        return $this->belongsToMany('App\Player', 'lineups_test', 'player_id', 'schedule_id');
    }
}
