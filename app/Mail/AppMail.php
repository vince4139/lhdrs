<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AppMail extends Mailable
{
    use Queueable, SerializesModels;

    public $received_from = 'sender@lhdrs.com';

    public $C_reply_to = 'info@lhdrs.com';

    public $received_to = '';

    public $received_subject = '';

    public $received_body = '';

    public $received_link = '';



    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($settings)
    {
        $this->process_infos($settings);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'title' => $this->received_subject,
            'body_text' => $this->received_body,
            'link' => $this->received_link
        ];
        return $this->from($this->received_from, 'Ligue de hockey de la rive sud')
                    ->to($this->received_to)
                    ->subject($this->received_subject)
                    ->view('emails.template', $data);
    }

    private function process_infos($settings) 
    {
        $this->set_to($settings['to']);
        $this->set_subject($settings['subject']);
        $this->set_body($settings['body']);
        $this->set_link($settings['link']);
    }

    private function set_to($to) {
        $this->received_to = $to;
    }

    private function set_subject($subject) {
        $this->received_subject = $subject;
    }

    private function set_body($body) {
        $this->received_body = $body;
    }

    private function set_link($link) {
        $this->received_link = $link;
    }
}
