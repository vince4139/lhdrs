<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'company',
        'phone',
        'address',
        'city',
        'zipcode',
        'newsletter',
        'emergency_contact_name',
        'emergency_contact_phone',
        'favorite_position',
        'activation_code',
        'username', 
        'email', 
        'password', 
        'ip_address',
        'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','ip_address'
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['player', 'roles'];

    public function player() {
        return $this->belongsTo('App\Player');
    }

    public function roles() {
        return $this->belongsToMany('App\Role', 'user_roles', 'user_id', 'role_id');
    }

    public function transactions() {
        return $this->hasMany('App\Transaction');
    }

    public function validateForPassportPasswordGrant($password)
    {
        return ($password === $this->password);
    }
}
