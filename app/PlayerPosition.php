<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerPosition extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'player_positions';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function player() {
        return $this->hasOne('App\Player');
    }
}
