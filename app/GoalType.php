<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoalType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'goal_types';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
