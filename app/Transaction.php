<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function player() {
    	return $this->belongsTo('App\Player');
    }

    public function marqueur() {
    	return $this->belongsTo('App\User');
    }

    public function ttype() {
        return $this->belongsTo('App\TransactionType');
    }
}
