<?php

if (!function_exists('get_translated_date')) {
	/**
     * Returns a translated date string
     *
     * @param string $date
     * The datestring to translate in french
     *
     * @param string $format
     * The format type of date that you want
     * @availableValues: 'short', 'long', 'long-days', 'long-months'
     *
     * @return string a date string in french
     *
     * */
	function get_translated_date($date, $format = 'long-days')
	{
		$days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
		$months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
		$short_days = ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'];
		$short_months = ['jan', 'fév', 'mar', 'avr', 'mai', 'jun', 'jui', 'aou', 'sep', 'oct', 'nov', 'déc'];

		$formated_date = "";

		$r_time = strtotime($date);
		$r_day = date('w', $r_time); 
		$r_month = date('n', $r_time) - 1; 
		$r_year = date('Y', $r_time); 

		switch ($format) {
			case 'short':
				$formated_date = $short_days[$r_day] . " " . date('d', $r_time) . " " . $short_months[$r_month];
				break;
			case 'long':
				$formated_date = $days[$r_day] . " " . date('d', $r_time) . " " . $months[$r_month];
				break;
			case 'long-days':
				$formated_date = $days[$r_day] . " " . date('d', $r_time) . " " . $short_months[$r_month];
				break;
			case 'long-months':
				$formated_date = $short_days[$r_day] . " " . date('d', $r_time) . " " . $months[$r_month];
				break;
			case 'no-day':
				$formated_date = date('d', $r_time) . " " . $months[$r_month];
				break;
			case 'no-day-short-months':
				$formated_date = date('d', $r_time) . " " . $short_months[$r_month];
				break;
			case 'no-day-short-months-year':
				$formated_date = date('d', $r_time) . " " . $short_months[$r_month] ." ". $r_year;
				break;
			default:
				$formated_date = $days[$r_day] . " " . date('d', $r_time) . " " . $months[$r_month];
		}


		return $formated_date;
	}
}

if (!function_exists('normalize_team_names')) {
	/**
     * Returns a translated date string
     *
     * @param string $name
     * Name string to normalize
     *
     * @return string normalized string
     *
     * */
	function normalize_team_names($name)
	{
		return ucfirst(strtolower($name));
	}
}

if (!function_exists('get_user_ip_addr')) {
	function get_user_ip_addr(){
	    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
	        //ip from share internet
	        $ip = $_SERVER['HTTP_CLIENT_IP'];
	    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
	        //ip pass from proxy
	        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    }else{
	        $ip = $_SERVER['REMOTE_ADDR'];
	    }
	    return $ip;
	}
}

function get_hash_pass($pass, $user) {
        if(gettype($user) !== 'object') {
            $salt = salt();
        } else if($user->count() > 0) {
            $salt = substr($user->password, 0, config('services.old_site.salt_length'));
        }

        return  $salt . substr(sha1($salt . $pass), 0, -config('services.old_site.salt_length'));
    }

function salt() {
    return substr(md5(uniqid(rand(), true)), 0, config('services.old_site.salt_length'));
}

function get_code() {
    return sha1(md5(microtime()));
}	
