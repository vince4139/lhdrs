<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class League extends Model
{


    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['season', 'status'];

    protected $casts = [
        'created_at' => 'datetime:Y',
    ];


    public function teams()
    {
    	return $this->hasMany('App\Team');
    }

    public function status()
    {
        return $this->belongsTo('App\LeagueStatus', 'status');
    }

    public function season()
    {
        return $this->belongsTo('App\Season');
    }

}
