<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * Get the Post's formtted date.
     *
     * @param  string  $value
     * @return string
     */
    public function getPublishedAttribute($value)
    {
        return [
            "original" => $value,
            "formatted" => ($value == 1) ? 'Publiée' : "Brouillon",
        ];
    }
}
