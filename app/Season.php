<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public $with = ['arena:id,name'];

    public function players() {
        return $this->belongsToMany('App\Player', 'inscriptions_test')->withPivot('is_goalie');
    }

    public function type()
    {
        return $this->belongsTo('App\SeasonType', 'type');
    }

    public function arena()
    {
        return $this->belongsTo('App\Arena');
    }
}
