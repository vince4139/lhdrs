<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
    
    public function user() {
    	return $this->hasOne('App\User');
    }

    public function goals() {
    	return $this->hasMany('App\Goal');
    }

    public function absences() {
    	return $this->hasMany('App\Absence');
    }

    public function transactions() {
        return $this->hasMany('App\Transaction');
    }

    public function position() {
        return $this->belongsTo('App\PlayerPosition', 'favorite_position');
    }

    public function seasons() {
        return $this->belongsToMany('App\Season', 'inscriptions_test');
    }

    public function games() {
        return $this->belongsToMany('App\Game', 'lineups_test', 'player_id', 'schedule_id');
    }

    public function teams() {
        return $this->belongsToMany('App\Team', 'teams_lineup_test');
    }
}
