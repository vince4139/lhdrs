<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Substitution extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $table = "substitution";

    public function player() {
    	return $this->belongsTo('App\Player');
    }
}
