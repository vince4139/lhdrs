<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Page;
use App\Menu;

use App\Helpers\CollectionHelper;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Affiche la page d'accueil
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $menus = Menu::with('menu_items')->get()->sortBy('order');
        return view('pages.home', [
            'main_menu' => Arr::flatten($menus->where('name', 'menu-principal-header'))[0]->menu_items,
            'footer_menu' => Arr::flatten( $menus->where('name', 'menu-footer'))[0]->menu_items
        ]);
    }


    public function list(Request $request) 
    {
        // return response()->json(Page::all());

        $filters = $request->columnFilters;
        $asked_page = $request->page;
        $results_per_page = ($request->perpage) ? $request->perpage : 15;
        $sort = json_decode($request->sort);
        

        if(!empty((array)$sort)) {
            $sort_field = $sort->field;
            $sort_type = $sort->type;

            // Funky way to sort, but collection sorting functions aren't smart enough
            $results = Page::all()->sort(function($a, $b) use ($sort_field, $sort_type) {
                $a_arr = $a->toArray();
                $b_arr = $b->toArray();

                if($sort_type == 'asc') {
                    return $b[$sort_field] < $a[$sort_field];
                } else {
                    return $b[$sort_field] > $a[$sort_field];
                }
            });

            $results = $results->flatten();
        } else {
            $results = Page::all()->flatten();
        }

        $total = $results->count();

        $paginated = CollectionHelper::paginate($results, $total, $results_per_page);

        return $paginated;
    }

    public function view()
    {

    }

    public function update(Request $request)
    {
        $page = Page::find($request->id);

        $page->league_id = $request->league_id;
        $page->name = $request->name;
        $page->slug_name = $request->slug_name;
        $page->content = $request->content;
        $page->content_more = $request->content_more;
        $page->order = $request->order;
        $page->child_of = $request->child_of;
        $page->type = $request->type;
        $page->published = $request->published;

        $page->save();

        return response()->json('La page ' . $page->name . ' a bien été mise à jour!');
    }

    public function publish(Request $request)
    {
        $page = Page::find($request->id);

        $page->published = 1;

        $page->save();

        return response()->json('La page ' . $page->name . ' a bien été publiée!');
    }

    public function unpublish(Request $request)
    {
        $page = Page::find($request->id);

        $page->published = 0;

        $page->save();

        return response()->json('La page ' . $page->name . ' a bien été dépubliée!');
    }

    public function create(Request $request)
    {   
        $page = new Page;

        $page->league_id = 1;
        $page->name = $request->name;
        $page->slug_name = $request->slug_name;
        $page->content = $request->content;
        $page->content_more = $request->content_more;
        $page->order = $request->order;
        $page->child_of = $request->child_of;
        $page->type = 1;
        $page->published = $request->published;

        $page->save();

        return response()->json('La page ' . $page->name . ' a bien été créé!');
    }

    public function destroy(Request $request)
    {
        $page = Page::find($request->id);
        $page_name = $page->name;

        $page->delete();

        return response()->json('La page ' . $page_name . ' a bien été supprimée!');
    }

}