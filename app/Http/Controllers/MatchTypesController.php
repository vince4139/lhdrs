<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\MatchType;

class MatchTypesController extends Controller
{
    public function list()
    {
        $types = MatchType::all();

        return response()->json($types);
    }
}
