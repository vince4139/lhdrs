<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Arena;

class ArenasController extends Controller
{
    public function list()
    {
        $arenas = Arena::all();

        return response()->json($arenas);
    }

    public function create()
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
