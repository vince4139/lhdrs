<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

use App\Helpers\CollectionHelper;

class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function list(Request $request) 
    {
        $filters = $request->columnFilters;
        $asked_page = $request->page;
        $results_per_page = ($request->perpage) ? $request->perpage : 15;
        $sort = json_decode($request->sort);
        

        if(!empty((array)$sort)) {
            $sort_field = $sort->field;
            $sort_type = $sort->type;

            // Funky way to sort, but collection sorting functions aren't smart enough
            $results = Post::all()->sort(function($a, $b) use ($sort_field, $sort_type) {
                $a_arr = $a->toArray();
                $b_arr = $b->toArray();

                if($sort_type == 'asc') {
                    return $b[$sort_field] < $a[$sort_field];
                } else {
                    return $b[$sort_field] > $a[$sort_field];
                }
            });

            $results = $results->flatten();
        } else {
            $results = Post::all()->flatten();
        }

        $total = $results->count();

        $paginated = CollectionHelper::paginate($results, $total, $results_per_page);

        return $paginated;
    }

    public function view()
    {

    }

    public function update(Request $request)
    {
        $post = Post::find($request->id);

        $post->league_id = $request->league_id;
        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->body = $request->body;
        $post->excerpt = $request->excerpt;
        $post->sticky = $request->sticky;
        $post->status = $request->status;
        $post->author_id = $request->author_id;

        $post->save();

        return response()->json('La nouvelle ' . $post->title . ' a bien été mise à jour!');
    }

    public function publish(Request $request)
    {
        $post = Post::find($request->id);

        $post->status = 'PUBLISHED';

        $post->save();

        return response()->json('La nouvelle ' . $post->title . ' a bien été publiée!');
    }

    public function unpublish(Request $request)
    {
        $post = Post::find($request->id);

        $post->status = 'DRAFT';

        $post->save();

        return response()->json('La nouvelle ' . $post->title . ' a bien été dépubliée!');
    }

    public function create(Request $request)
    {   
        $post = new Post;

        $post->league_id = $request->league_id;
        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->body = $request->body;
        $post->excerpt = $request->excerpt;
        $post->sticky = $request->sticky;
        $post->status = $request->status;
        $post->author_id = $request->author_id;

        $post->save();

        return response()->json('La nouvelle ' . $post->title . ' a bien été créé!');
    }

    public function destroy(Request $request)
    {
        $post = Post::find($request->id);
        $post_name = $post->title;

        $post->delete();

        return response()->json('La nouvelle ' . $post_name . ' a bien été supprimée!');
    }

}