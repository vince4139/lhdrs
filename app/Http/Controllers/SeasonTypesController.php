<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SeasonType;

class SeasonTypesController extends Controller
{
    public function list()
    {
        $types = SeasonType::all();

        return response()->json($types);
    }

    public function create()
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
