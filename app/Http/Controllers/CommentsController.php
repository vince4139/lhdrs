<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

use App\Helpers\CollectionHelper;

class CommentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function list(Request $request) 
    {
        $filters = $request->columnFilters;
        $asked_page = $request->page;
        $results_per_page = ($request->perpage) ? $request->perpage : 15;
        $sort = json_decode($request->sort);
        

        if(!empty((array)$sort)) {
            $sort_field = $sort->field;
            $sort_type = $sort->type;

            // Funky way to sort, but collection sorting functions aren't smart enough
            $results = Comment::all()->sort(function($a, $b) use ($sort_field, $sort_type) {
                $a_arr = $a->toArray();
                $b_arr = $b->toArray();

                if($sort_type == 'asc') {
                    return $b[$sort_field] < $a[$sort_field];
                } else {
                    return $b[$sort_field] > $a[$sort_field];
                }
            });

            $results = $results->flatten();
        } else {
            $results = Comment::all()->flatten();
        }

        $total = $results->count();

        $paginated = CollectionHelper::paginate($results, $total, $results_per_page);

        return $paginated;
    }

    public function view()
    {

    }

    public function update(Request $request)
    {
        $comment = Comment::find($request->id);

        

        $comment->save();

        return response()->json('Le commentaire de ' . $comment->name . ' a bien été mise à jour!');
    }

    public function create(Request $request)
    {   
        $comment = new Comment;


        $comment->save();

        return response()->json('La nouvelle ' . $comment->title . ' a bien été créé!');
    }

    public function destroy(Request $request)
    {
        $comment = Comment::find($request->id);
        $comment_name = $comment->title;

        $comment->delete();

        return response()->json('La nouvelle ' . $comment_name . ' a bien été supprimée!');
    }

}