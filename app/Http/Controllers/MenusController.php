<?php

namespace App\Http\Controllers;

use App\Menu;
use App\MenuItem;
use Illuminate\Http\Request;
use App\Helpers\CollectionHelper;

class MenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $filters = $request->columnFilters;
        $asked_page = $request->page;
        $results_per_page = ($request->perpage) ? $request->perpage : 15;
        $sort = json_decode($request->sort);
        

        if(!empty((array)$sort)) {
            $sort_field = $sort->field;
            $sort_type = $sort->type;

            // Funky way to sort, but collection sorting functions aren't smart enough
            $results = Menu::all()->sort(function($a, $b) use ($sort_field, $sort_type) {
                $a_arr = $a->toArray();
                $b_arr = $b->toArray();

                if($sort_type == 'asc') {
                    return $b[$sort_field] < $a[$sort_field];
                } else {
                    return $b[$sort_field] > $a[$sort_field];
                }
            });

            $results = $results->flatten();
        } else {
            $results = Menu::all();
        }

        $total = $results->count();

        $paginated = CollectionHelper::paginate($results, $total, $results_per_page);

        return $paginated;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function menuItemsList($menu_id)
    {
        return MenuItem::where('menu_id', $menu_id)->get()->sortBy('order')->flatten();
    }

    public function menuItemCreate(Request $request)
    {
        $menu_item = new MenuItem;

        $menu_item->menu_id = $request->menu_id;
        $menu_item->title = $request->title;
        $menu_item->url = $request->url;
        $menu_item->icon_class = $request->icon_class;
        $menu_item->parent_id = 0;
        $menu_item->order = $request->order;

        $menu_item->save();

        return response()->json("L'élément " . $menu_item->title . ' a bien été créé!');
    }

    public function menuItemUpdate(Request $request, $menu_id, $menu_item_id)
    {
        $menu_item = MenuItem::find($menu_item_id);

        $menu_item->menu_id = $request->menu_id;
        $menu_item->title = $request->title;
        $menu_item->url = $request->url;
        $menu_item->icon_class = $request->icon_class;
        $menu_item->parent_id = $request->parent_id;
        $menu_item->order = $request->order;

        $menu_item->save();

        return response()->json("L'élément " . $menu_item->title . ' a bien été mis à jour!');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function menuItemDelete($menu_id, $menu_item_id)
    {
        $menu_item = MenuItem::find($menu_item_id);
        $menu_item_name = $menu_item->title;

        $menu_item->delete();

        return response()->json("L'élément " . $menu_item_name . ' a bien été supprimée!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menu = new Menu;

        $menu->name = $request->name;

        $menu->save();

        return response()->json('Le Menu ' . $menu->name . ' a bien été créé!');
    }

    /**
     * Return the specified resource.
     *
     * @param  int  $id
     * @return json of the ressource
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menu = Menu::find($request->id);

        $menu->name = $request->name;

        $menu->save();

        return response()->json('Le Menu ' . $menu->name . ' a bien été mise à jour!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return message
     */
    public function destroy(Request $request)
    {
        $menu = Menu::find($request->id);
        $menu_name = $menu->name;

        $menu->delete();

        return response()->json('Le Menu ' . $menu_name . ' a bien été supprimée!');
    }
}
