<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Menu;

class DashboardController extends Controller
{
    public function adminMenu(Request $request) 
    {
        $menu = Menu::with('menu_items')->get();

        return json_encode(Arr::flatten($menu->where('name', 'admin'))[0]->menu_items->sortBy('order')->flatten());
    }
}