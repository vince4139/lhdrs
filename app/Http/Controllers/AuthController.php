<?php

namespace App\Http\Controllers;

use App\User;
use App\Mail\AppMail;
use App\Http\Requests\StoreUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use TCG\Voyager\Models\Page;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $user = User::where('email', '=', $request->username)->first();

        if($user->active == 0) {
            return response()->json("Your account isn't active", 401);
        }

        $http = new \GuzzleHttp\Client;
        try {
            $response = $http->post(config('services.passport.login_endpoint'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->username,
                    'password' => get_hash_pass($request->password, $user),
                ]
            ]);

            return $response->getBody();

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            if ($e->getCode() === 400) {
                
                return response()->json("Requête invalide, veuillez entrer un nom d'utilisateur et un mot de passe", $e->getCode());

            } else if ($e->getCode() === 401) {
                return response()->json('Les informations que vous avez entrées sont incorrectes', $e->getCode());
            }
            return response()->json('Quelque chose ne marche pas du côté du serveur, veuillez revenir plus tard!', $e->getCode());
        }
    }

    public function register(StoreUser $request)
    {
        $request->validated();

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'username' => strtolower($request->first_name . ' ' . $request->last_name),
            'favorite_position' => $request->favorite_position,
            'phone' => $request->phone,
            'emergency_contact_name' => $request->emergency_contact_name,
            'emergency_contact_phone' => $request->emergency_contact_phone,
            'address' => $request->address,
            'city' => $request->city,
            'zipcode' => $request->zipcode,
            'email' => $request->email,
            'password' => get_hash_pass($request->password, []),
            'ip_address' => inet_pton(get_user_ip_addr()),
            'role_id' => 4,
            'active' => 0,
            'activation_code' => $this->get_code(),
            'newsletter' => $request->newsletter
        ]);

        // if(!is_null($user)) {
        //     $mail_data = [
        //         'to'=> $user->email,
        //         'subject'=> '[LHDRS] - Activation de votre compte',
        //         'body'=> '<p>Bonjour ' . $user->first_name . ' ' .$user->last_name. ',</p><p>Pour valider votre courriel, veuillez vous rendre sur la page suivante:</p>',
        //         'link' => url("/activate/".$user->id."/".$user->activation_code),
        //     ];

        //     // Mail::send(new AppMail($mail_data));
            return response()->json('Votre compte doit maintenant être activé, allez consulter vos courriel pour la suite!', 200);
        // }

        
    }
    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });
        return response()->json('Logged out successfully', 200);
    }

    public function activate(Request $request) {
        $user = User::find($request->id);

        if(!is_null($user)) {
            if($user->activation_code === $request->code) {
                $user->active = 1;
                $user->activation_code = NULL;
                $user->save();

                return response()->json([
                    "messageClass" => 'message-box success', 
                    "message" => "Votre compte est maintenant actif!"
                ], 200);
            }
        } else {
            return response()->json([
                "messageClass" => 'message-box error', 
                "message" => "Aucun utilisateur ne correspond à la requête demandée!"
            ], 422);
        }
    }
}
