<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;

use App\Helpers\CollectionHelper;

use App\Absence;
use App\Game;
use App\Gamesheet;
use App\Player;
use App\PlayerPosition;
use App\League;
use App\LeagueType;
use App\Season;
use App\Team;


class ScheduleController extends Controller
{
    protected $player_teams;
    protected $player_leagues;
    protected $player_games;
    protected $player_absences;
    protected $player_past_games;

    public function __construct() {
    }

    private function _format_dates($data, $format) {
        $formatted_data = $data;
        foreach ($formatted_data as $key => $value) {
            foreach ($formatted_data[$key] as $item) {
                $item->game->date = get_translated_date($item->game->date, $format);
            }
        }
        return $formatted_data;
    }

    private function _get_player_absences ($player_id) {

        $absences = Absence::where('player_id', $player_id);

        $absences_sorted = [
            'player' => $absences->whereIn('player_position', 0)->map(function($item, $key) { 
                return $item->game_id;

            })->filter()->flatten(),
            'goalie' => $absences->whereIn('player_position', 1)->map(function($item, $key) { 
                return $item->game_id; 
            })->filter()->flatten()
        ];

        return $absences_sorted;
    }

    private function _get_player_games ($player_id, $sortDesc = false) {
        $results = [
            'player' => collect(),
            'goalie' => collect()
        ];

        foreach ($results as $position => $value) {
            if(!is_null($this->player_teams)) {
                
                if(count($this->player_teams[$position]) > 0) {
                    foreach ($this->player_teams[$position] as $team) {
                        $games_away = $this->games->where('away', $team->id)->flatten();
                        $games_home = $this->games->where('home', $team->id)->flatten();

                        if(count($games_away) > 0) {
                            foreach ($games_away as $game) {
                                $results[$position]->push($game);
                            }
                        }

                        if(count($games_home) > 0) {
                            foreach ($games_home as $game) {
                                $results[$position]->push($game);
                            }
                        }
                    }
                } else {
                    continue;
                }
            }

            $results_home = $this->games->whereIn('home_' . $position, $player_id)->flatten();
            foreach ($results_home as $game) {
               $results[$position]->push($game);
            }

            $results_sub_home = $this->games->whereIn('home_' . $position . '_subs', $player_id)->flatten();
            foreach ($results_sub_home as $game) {
               $results[$position]->push($game);
            }

            $results_away = $this->games->whereIn('away_' . $position, $player_id)->flatten();
            foreach ($results_away as $game) {
               $results[$position]->push($game);
            }

            $results_sub_away = $this->games->whereIn('away_' . $position . '_subs', $player_id)->flatten();
            foreach ($results_sub_away as $game) {
               $results[$position]->push($game);
            }


            if(!is_null($this->player_absences)) {
                if(count($this->player_absences[$position]) > 0) {
                    foreach ($this->player_absences[$position] as $absence) {
                        $results[$position] = $results[$position]->reject(function($value, $key) use ($absence) {
                            return $value->id == $absence;
                        });
                    }
                }
            }


            

            if($sortDesc) {
                $results[$position] = $results[$position]->sortByDesc('date')->flatten();
            } else {
                $results[$position] = $results[$position]->sortBy('date')->flatten();
            }
        }

        return $results;
    }

    private function _get_player_past_games ($player_id, $sortDesc = true) {
        $past_games = [
            'player' => collect(),
            'goalie' => collect()
        ];


        $gamesheets = Gamesheet::with('game.league')->get();

        foreach ($past_games as $position => $value) {
            if($position == 'player') {
                $past_games[$position] = $past_games[$position]->merge($gamesheets->whereIn('team_away_'.$position.'s', $player_id));
                $past_games[$position] = $past_games[$position]->merge($gamesheets->whereIn('team_home_'.$position.'s', $player_id));
            } else {
                $past_games[$position] = $past_games[$position]->merge($gamesheets->whereIn('team_away_'.$position, $player_id));
                $past_games[$position] = $past_games[$position]->merge($gamesheets->whereIn('team_home_'.$position, $player_id));
            }

            if($sortDesc) {
                $past_games[$position] = $past_games[$position]->sortByDesc(function($gamesheet, $key) {
                    return $gamesheet->game->date;
                })->flatten();
            }
        }

        return $past_games;
    }

    private function _get_player_leagues($player_id, $active = true) {
        $status_query = ($active) ? [1,3] : 2;
        $collection = [
            'player' => [],
            'goalie' => []
        ];

        $results = [
            'player' => [],
            'goalie' => []
        ];

        if(!is_null($this->player_teams)) {
            foreach ($this->player_teams as $position => $teams) {
                foreach ($teams as $team) {
                    array_push($results[$position], League::where('id', $team->league_id));
                }
            }
        }

        foreach (League::all() as $league) {
            $collection['player'] = explode(',', $league['players']);
            $collection['goalie'] = explode(',', $league['goalie']);

            foreach($collection as $position => $id) {
                if(in_array($player_id, $id)) {
                    array_push($results[$position], $league);
                }
            }      
        }

        return $results;
    }

    private function _get_player_teams($player_id) 
    {
        $teams = Team::all();
        $player_teams = [
            'player' => $teams->whereIn('players', $player_id)->flatten(),
            'goalie' => $teams->whereIn('goalie', $player_id)->flatten()
        ];
        return $player_teams;
    }

    public function history($player_id) {
        $this->player_past_games = self::_get_player_past_games($player_id);

        return json_encode($this->player_past_games);
    }

    public function player($player_id) {
        $this->player_teams = self::_get_player_teams($player_id);
        $this->player_leagues = self::_get_player_leagues($player_id, false);
        $this->player_absences = self::_get_player_absences($player_id);


        $this->player_games = self::_get_player_games($player_id, true);
    	return json_encode($this->player_games);
    }

    public function nextFive($player_id) {

    }

    public function lastFive($player_id) {
        $this->player_past_games = self::_get_player_past_games($player_id);

        $last_five = Arr::flatten(self::_format_dates($this->player_past_games, 'no-day'));

        return json_encode(array_slice($last_five, 0, 5));
    }

    public function generateSeasonGames(Request $request, $season_id)
    {
        $results = [
            "class" => 'success',
            "message" => '',
            "game_created" => 0
        ];

        $days_of_week = [
            "Dimanche" => 'Sunday', 
            "Lundi" => 'Monday', 
            "Mardi" => 'Tuesday', 
            "Mercredi" => 'Wednesday', 
            "Jeudi" => 'Thursday', 
            "Vendredi" => 'Friday', 
            "Samedi" => 'Saturday'
        ];

        $season_gpw = (is_array($request->games_per_week)) ? $request->games_per_week : json_decode($request->games_per_week, true);

        $begin_date = date('Y-m-d', strtotime($request->begin));
        $games = $request->games_number;
        $time_jump = '+1 week';
        $is_game_limited = false;

        if(isset($request->end)) {
            // Si une date de fin a été fournie, la transforme en timestamp unix
            $end_date = date('Y-m-d', strtotime($request->end));
        } else {
            $end_date = strtotime('+ 1 year', strtotime($request->begin));
            $is_game_limited = true;
        }

        // // création de variables pour la boucle
        $loop_begin = strtotime($begin_date);
        $loop_end = strtotime($end_date);
        
        for($i = $loop_begin; $i <= $loop_end; $i = strtotime($time_jump, $i)) 
        {
            foreach ($season_gpw as $game) {
                $new_game = new Game;

                $new_game->match_type = 1;
                $new_game->date = date('Y-m-d', strtotime($days_of_week[$game['day']], $i));
                $new_game->game_start = $game['start'];
                $new_game->game_end = $game['end'];
                $new_game->arena_id = $request->arena_id;
                $new_game->season_id = $season_id;

                $new_game->save(); 

                if(count($request->players) > 0) {

                    foreach ($request->players as $player) {
                        $new_game->players()->attach($player['id'], [
                            'is_goalie' => $player['pivot']['is_goalie'],
                            'schedule_id' => $new_game->id,
                            'player_id' => $player['id']
                        ]);
                    }
                    
                } 

                $results["game_created"]++; 
            }

            if($is_game_limited && $results["game_created"] == $games) {
                $new_game->league_id = $request->league_id;
                $break;
            }
        }

        if($results["game_created"] > $games) 
        {
            $results["message"] = "Selon les dates fournies au système, un nombre excédentaire de parties ont été générée";
        } 
        else if($results["game_created"] < $games) 
        {
            $results["message"] = "Selon les dates fournies au système, un nombre insuffisant de parties ont été générée";
        }
        else {
            $results["message"] = "Les matchs ont été créés avec succès";
        }


        return response()->json($results);
    }

    public function createSingleGame(Request $request, $season_id)
    {
        $new_game = new Game;

        $new_game->match_type = $request->match_type;
        $new_game->date = date('Y-m-d', strtotime($request->date));
        $new_game->game_start = $request->game_start;
        $new_game->game_end = $request->game_end;
        $new_game->arena_id = $request->arena_id;
        $new_game->league_id = $request->league_id;
        $new_game->season_id = $season_id;

        $new_game->save();

        return response()->json('La partie du ' . $new_game->date . ' a bien été créé!');
    }

    public function updateSingleGame(Request $request, $season_id, $game_id)
    {
        $game = Game::find($game_id);

        $game->match_type = $request->match_type;
        $game->date = date('Y-m-d', strtotime($request->date));
        $game->game_start = $request->game_start;
        $game->game_end = $request->game_end;
        $game->arena_id = $request->arena_id;
        $game->league_id = $request->league_id;
        $game->season_id = $season_id;

        $game->save();

        return response()->json('La partie du ' . $game->date . ' a bien été mise à jour!');
    }

    public function getSeasonGames(Request $request, $season_id)
    {
        // $games = Game::where('season_id', $season_id)->get();

        // return response()->json($games);
        $filters = $request->columnFilters;
        $asked_page = $request->page;
        
        $sort = json_decode($request->sort);
        

        if(!empty((array)$sort)) {
            $sort_field = $sort->field;
            $sort_type = $sort->type;
            // Funky way to sort, but collection sorting functions aren't smart enough
            $results = Game::where('season_id', $season_id)->get()->sort(function($a, $b) use ($sort_field, $sort_type) {
                $a_arr = $a->toArray();
                $b_arr = $b->toArray();

                if($sort_type == 'asc') {
                    return $b[$sort_field] < $a[$sort_field];
                } else {
                    return $b[$sort_field] > $a[$sort_field];
                }
            });

            $results = $results->flatten();
        } else {
            $results = Game::where('season_id', $season_id)->get()->flatten();
        }

        $total = $results->count();

        if($request->perpage) {
            $results_per_page = $request->perpage;
        } else if($total > 0) {
            $results_per_page = $total;
        } else {
            $results_per_page = 15;
        }

        $paginated = CollectionHelper::paginate($results, $total, $results_per_page);

        return $paginated;
    }

    public function destroySeasonGame($season_id, $game_id)
    {
        $game = Game::find($game_id);
        $game_date = date('Y-m-d', strtotime($game->date)) . " " . $game->game_start;

        $game->delete();
        return response()->json('Le match prévue le ' . $game_date . ' a bien été supprimée!');
    }
}
