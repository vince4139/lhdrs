<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlayerStat;
use App\Game;
use App\Team;


class StatsController extends Controller
{

    public function players()
    {
        $player_stats = PlayerStat::where('position', 1)->paginate(50);
        return $player_stats->toJson();
    }

    public function player($id)
    {
        //
    }

    public function goalies()
    {
        //
    }

    public function goalie($id)
    {
        //
    }

    public function lastFive()
    {
        $games = Game::take(5)->has('gamesheet')->orderBy('date', 'desc')->get();
        $gamesheets = [];
        $attr_to_hide = [
            'arbitre',
            'juge',
            'chrono',
            'user_id',
            'team_home_players',
            'team_away_players', 
            'team_home_players_numbers', 
            'team_away_players_numbers',
            'team_home_goalie',
            'team_home_absents',
            'team_away_goalie',
            'team_away_absents',
            'team_home_shots_1',
            'team_home_shots_2',
            'team_home_shots_3',
            'team_home_shots_ov',
            'team_home_shotout',
            'team_away_shots_1',
            'team_away_shots_2',
            'team_away_shots_3',
            'team_away_shots_ov',
            'team_away_shotout',
            'star_three',
            'star_two',
            'star_one',
        ];

        foreach ($games as $g) {
            $g->gamesheet->start_at = $g->game_start;
            $g->gamesheet->date = $g->date;
            $g->gamesheet->translated_date = get_translated_date($g->date);

            $g->gamesheet->team_home = is_numeric($g->gamesheet->team_home) ? Team::find(intval($g->gamesheet->team_home))['name'] : $g->gamesheet->team_home;
            $g->gamesheet->team_away = is_numeric($g->gamesheet->team_away) ? Team::find(intval($g->gamesheet->team_away))['name'] : $g->gamesheet->team_away;

            array_push($gamesheets, $g->gamesheet->makeHidden($attr_to_hide));
        }

        usort($gamesheets, function($a, $b) {
            return strtotime($a['date']) <=> strtotime($b['date']);
        });

        return $gamesheets;
    }

    public function scoresheet($id) {
        $game = Game::find($id);

    }
   
}
