<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\LeagueStatus;

class LeagueStatusesController extends Controller
{
    public function list()
    {
        $statuses = LeagueStatus::all();

        return response()->json($statuses);
    }

    public function create()
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
