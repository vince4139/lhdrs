<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\League;

use App\Helpers\CollectionHelper;

class LeaguesController extends Controller
{
    const ORG_ARR = [3000, 1000, 2000];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $filters = $request->columnFilters;
        $asked_page = $request->page;
        $results_per_page = ($request->perpage) ? $request->perpage : 15;
        $sort = json_decode($request->sort);
        

        if(!empty((array)$sort)) {
            $sort_field = $sort->field;
            $sort_type = $sort->type;
            // Funky way to sort, but collection sorting functions aren't smart enough
            $results = League::all()->sort(function($a, $b) use ($sort_field, $sort_type) {
                $a_arr = $a->toArray();
                $b_arr = $b->toArray();

                if($sort_type == 'asc') {
                    return $b[$sort_field] < $a[$sort_field];
                } else {
                    return $b[$sort_field] > $a[$sort_field];
                }
            });

            $results = $results->flatten();
        } else {
            $results = League::all()->flatten();
        }

        $total = $results->count();

        $paginated = CollectionHelper::paginate($results, $total, $results_per_page);

        return $paginated;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $league = new League;

        $league->name = $request->name;
        $league->publicity_home = $request->publicity_home;
        $league->publicity_away = $request->publicity_away;
        // $league->season_id = $request->season_id;
        $league->status = $request->status;

        $latest_league = League::where('status', '=', $request->status)->take(1)->orderBy('leagues_order', 'DESC')->get();
        $latest_order = (count($latest_league) > 0) ? $latest_league[0]->leagues_order - self::ORG_ARR[$request->status - 1]: 1;

        $league->leagues_order = ($latest_order + 1) + (self::ORG_ARR[$request->status - 1]);


        $league->save();

        return response()->json('La ligue ' . $league->name . ' a bien été créé!');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $league = League::find($request->id);

        $league_order = $league->leagues_order - self::ORG_ARR[$league->status - 1];

        $league->name = $request->name;
        $league->publicity_home = $request->publicity_home;
        $league->publicity_away = $request->publicity_away;
        // $league->season_id = $request->season_id;
        $league->status = $request->status;

        if($request->status !== $league->status) {
            $league->leagues_order = $league_order + self::ORG_ARR[$request->status - 1];
        }

        $league->save();

        return response()->json('La ligue ' . $league->name . ' a bien été mise à jour!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $league = League::find($request->id);
        $league_name = $league->name;

        $league->delete();

        return response()->json('La ligue ' . $league_name . ' a bien été supprimée!');
    }

    public function modifyOrder(Request $request, $id)
    {
        $league = League::find($request->id);

        $league_after = League::where('leagues_order', '>', $league->leagues_order)->take(1)->get();
        $league_before = League::where('leagues_order', '<', $league->leagues_order)->take(1)->orderBy('leagues_order', 'DESC')->get();

        $action = explode('/', $request->path());

        if(end($action) == 'increase') {
            if(count($league_after) > 0) {
                $league->leagues_order = $league_after[0]->leagues_order + 1;
                $league_after[0]->leagues_order = $league->leagues_order - 1;

                $league_after[0]->save();
            } else {
                $league->leagues_order = $league->leagues_order + 1;
            }

            $league->save();
            
        } else {
            if(count($league_before) > 0) {
                $league->leagues_order = $league_before[0]->leagues_order - 1;
                $league_before[0]->leagues_order = $league->leagues_order + 1;
                
                $league_before[0]->save();
            } else {
                $league->leagues_order = $league->leagues_order - 1;
            }

            $league->save();
            
        }

        return response()->json("L'ordre de la ligue " . $league->name . ' a bien été mise à jour!');
    }
}
