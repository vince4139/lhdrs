<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\CollectionHelper;

use App\Season;

class SeasonsController extends Controller
{

    const ORG_ARR = [3000, 1000, 2000];

    public function list(Request $request)
    {

        $filters = $request->columnFilters;
        $asked_page = $request->page;
        $results_per_page = ($request->perpage) ? $request->perpage : 15;
        $sort = json_decode($request->sort);
        

        if(!empty((array)$sort)) {
            $sort_field = $sort->field;
            $sort_type = $sort->type;
            // Funky way to sort, but collection sorting functions aren't smart enough
            $results = Season::with(['type','players:players.id,first_name,last_name'])->get()->sort(function($a, $b) use ($sort_field, $sort_type) {
                $a_arr = $a->toArray();
                $b_arr = $b->toArray();

                if($sort_type == 'asc') {
                    return $b[$sort_field] < $a[$sort_field];
                } else {
                    return $b[$sort_field] > $a[$sort_field];
                }
            });

            $results = $results->flatten();
        } else {
            $results = Season::with(['type','players:players.id,first_name,last_name'])->get()->flatten();
        }

        $total = $results->count();

        $paginated = CollectionHelper::paginate($results, $total, $results_per_page);

        return $paginated;
    }

    public function create(Request $request)
    {
        $season = new Season;

        // print_r($request->game_start);

        $season->name = $request->name;
        $season->type = $request->type;
        $season->begin = $request->begin;
        $season->end = $request->end;
        $season->games_per_week = json_encode($request->games_per_week);
        $season->arena_id = $request->arena_id;
        $season->notes = $request->notes;
        $season->level = $request->level;
        $season->publicity_id_home = $request->publicity_id_home;
        $season->publicity_id_away = $request->publicity_id_home;
        $season->player_season_price = $request->player_season_price;
        $season->goalie_season_price = $request->goalie_season_price;
        $season->player_game_price = $request->player_game_price;
        $season->goalie_game_price = $request->goalie_game_price;
        $season->games_number = $request->games_number;
        $season->league_id = $request->league_id;
        $season->inscription_players = $request->inscription_players;
        $season->inscription_goalies = $request->inscription_goalies;
        $season->find_match = $request->find_match;
        $season->total_spots = $request->total_spots;
        $season->total_spots_goalies = $request->total_spots_goalies;
        $season->status = ($request->status === null) ? 1 : $request->status;


        $latest_season = Season::where('status', '=', $request->status)->take(1)->orderBy('order', 'DESC')->get();
        $latest_order = (count($latest_season) > 0) ? $latest_season[0]->order - self::ORG_ARR[$request->status - 1]: 1;

        $season->order = ($latest_order + 1) + (self::ORG_ARR[$season->status - 1]);

        $season->save();
        return response()->json('La saison ' . $season->name . ' a bien été créée!');
    }

    public function leagueSeasons(Request $request, $league_id)
    {
        $seasons = Season::where('league_id', $league_id)->with(['type','players:players.id,first_name,last_name'])->get()->flatten();

        return response()->json($seasons);
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $season = Season::find($id);

        $season_order = $season->order - self::ORG_ARR[$season->status - 1];

        $season->name = $request->name;
        $season->type = $request->type;
        $season->begin = date("Y-m-d", strtotime($request->begin));
        $season->end = date("Y-m-d", strtotime($request->end));
        $season->games_per_week = json_encode($request->games_per_week);
        $season->arena_id = $request->arena_id;
        $season->notes = $request->notes;
        $season->level = $request->level;
        $season->publicity_id_home = $request->publicity_id_home;
        $season->publicity_id_away = $request->publicity_id_away;
        $season->player_season_price = $request->player_season_price;
        $season->goalie_season_price = $request->goalie_season_price;
        $season->player_game_price = $request->player_game_price;
        $season->goalie_game_price = $request->goalie_game_price;
        $season->games_number = $request->games_number;
        $season->league_id = $request->league_id;
        $season->total_spots = $request->total_spots;
        $season->total_spots_goalies = $request->total_spots_goalies;
        $season->inscription_players = $request->inscription_players;
        $season->inscription_goalies = $request->inscription_goalies;
        $season->find_match = $request->find_match;
        $season->status = $request->status;


        if(isset($request->players)) {
            $simplifiedPlayers = [];

            foreach ($request->players as $player) {
                $simplifiedPlayers[$player["id"]] = ['is_goalie' => $player['pivot']['is_goalie']];
            }
            $season->players()->sync($simplifiedPlayers);
        }


        if($request->status !== $season->status) {
            $season->order = $season_order + self::ORG_ARR[$request->status - 1];
        }

        $season->save();
        return response()->json('La saison ' . $season->name . ' a bien été mise à jour!');

    }

    public function destroy($id)
    {
        $season = Season::find($id);
        $season_name = $season->name;

        $season->delete();
        return response()->json('La saison ' . $season_name . ' a bien été supprimée!');
    }

    public function modifyOrder(Request $request, $id)
    {
        $season = Season::find($request->id);

        $season_after = Season::where('order', '>', $season->order)->take(1)->get();
        $season_before = Season::where('order', '<', $season->order)->take(1)->orderBy('order', 'DESC')->get();

        $action = explode('/', $request->path());

        if(end($action) == 'increase') {
            if(count($league_after) > 0) {
                $season->order = $season_after[0]->order + 1;
                $season_after[0]->order = $league->order - 1;

                $season_after[0]->save();
            } else {
                $season->order = $season->order + 1;
            }

            $season->save();
            
        } else {
            if(count($season_before) > 0) {
                $season->order = $season_before[0]->order - 1;
                $season_before[0]->order = $league->order + 1;
                
                $season_before[0]->save();
            } else {
                $season->order = $season->order - 1;
            }

            $season->save();
            
        }

        return response()->json("L'ordre de la ligue " . $season->name . ' a bien été mise à jour!');
    }
}
