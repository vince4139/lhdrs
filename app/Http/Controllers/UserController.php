<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;

use App\User;


class UserController extends Controller
{
	private function _calculateBalance($user) {
		$result= 0;
        $transactions = ($user->role_id === 3) ? $user->transactions : $user->player->transactions;


		foreach ($transactions as $t) {
            
			if($t->type === 1) {
				$result += floatval($t->amount); 
			} else {
				$result -= floatval($t->amount);
			}
		}
		return number_format($result, 2, ',', ' ') . '$';
	}

    private function _getSchedule($user_id)
    {
        
    }


    public function user(Request $request) 
    {
    	$user = $request->user();


    	$user->balance = self::_calculateBalance($user);;
        $user->schedule = self::_getSchedule($user->id);

    	return json_encode($user);
    }
}
