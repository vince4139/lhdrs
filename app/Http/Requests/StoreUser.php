<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|regex:/^([^0-9]*)$/|max:255',
            'last_name' => 'required|regex:/^([^0-9]*)$/|max:255',
            'favorite_position' => 'required',
            'phone' => 'required',
            'emergency_contact_name' => 'required|string|max:255',
            'emergency_contact_phone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'zipcode' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'rules' => 'accepted',
        ];
    }

    public function messages() {
        return [
            'first_name.required' => "Le prénom est requis",
            'first_name.regex' => "Le prénom n'est pas du bon format",
            'first_name.max' => "Le prénom est trop long",
            'last_name.required' => "Le nom est requis",
            'last_name.regex' => "Le nom n'est pas du bon format",
            'last_name.max' => "Le nom est trop long",
            'favorite_position.required' => 'La position favorite est requise',
            'emergency_contact_name.required' => "Le nom du contact d'urgence est requis",
            'emergency_contact_name.string' => "Le format du nom du contact d'urgence n'est pas valide",
            'emergency_contact_name.max' => "Le nom du contact est trop long",
            'emergency_contact_phone.required' => "Le numéro de contact d'urgence est requis",
            'address.required' => 'Votre adresse est requise',
            'city.required' => 'Votre ville est requise',
            'zipcode.required' => 'Votre code postal est requis',
            'email.required' => 'Votre adresse courriel est requise',
            'email.string' => 'Votre adresse courriel ne doit pas être numérique',
            'email.email' => "Le format de votre adresse courriel n'est pas valide",
            'email.max' => "L'adresse courriel entrée est trop longue",
            'email.unique' => "L'adresse courriel entrée est déjà utilisée",
            'password.required' => 'Le mot de passe est requis',
            'password.string' => "Le format du mot de passe n'est pas valide",
            'password.min' => "Le mot de passe entré est trop court, il doit contenir au moins 6 caractères",
            'password.confirmed' => "Votre mot de passe doit être identique",
            'rules.accepted' => 'Vous devez accepter les règlements de la LHDRS',
        ];
    }
}
