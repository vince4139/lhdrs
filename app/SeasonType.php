<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeasonType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'season_types';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
