<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeagueStatus extends Model
{
        /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'league_statuses';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
