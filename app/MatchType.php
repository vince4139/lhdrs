<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'match_types';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
