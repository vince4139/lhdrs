<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	/**
     * Get the Post's formtted date.
     *
     * @param  string  $value
     * @return string
     */
    public function getEntryDateAttribute($value)
    {
        return [
            "original" => $value,
            "formatted" => get_translated_date($value, 'no-day-short-months-year')
        ];
    }
}
