<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameReport extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    public function game() {
    	return $this->belongsTo('App\Game');
    }
}
