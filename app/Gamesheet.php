<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gamesheet extends Model
{

	protected $primaryKey = 'game_id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    public function game() {
    	return $this->belongsTo('App\Game');
    }
}
