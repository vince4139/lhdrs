<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicityFormat extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'publicity_formats';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
