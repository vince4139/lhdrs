import Vue from 'vue'
import Vuex from 'vuex'

import createPersistedState from 'vuex-persistedstate'
import * as Cookies from 'js-cookie'

Vue.use(Vuex)

export const store = new Vuex.Store({
	plugins: [
		createPersistedState({
			getState: (key) => Cookies.getJSON(key),
	      	setState: (key, state) => Cookies.set(key, state, { expires: 3, secure: true })
	    })
	],
	state : {
		token: localStorage.getItem('access_token') || null,
		user: null,
		lastFives: [],
		loading: false
	},
	getters: {
		lastFives(state) { 
			return state.lastFives
		},
		isLoading(state) {
			return state.loading
		},
		loggedIn(state) {
			return state.token !== null
		},
		userFullName(state) {
			if(state.user !== null) {
				return state.user.first_name + " " + state.user.last_name;
			}
		},
		userBalance(state) {
			if(state.user !== null) {
				return state.user.balance;
			}
		},
		token(state) {
			return state.token;
		},
		userPlayerId(state) {
			if(state.user !== null) {
				return state.user.player_id;
			}
		},
		userLoaded(state) {
			return state.user !== null;
		},
		isUserAdmin(state) {
			if(state.user !== null) {
				return (state.user.roles.filter(function(e) { return e.name === 'Admin' || e.name === 'SuperAdmin'; }).length > 0) 
			}
		}
	},
	mutations: {
		deleteToken(state){
			state.token = null;
		},
		deleteUser(state){
			state.user = null;
		},
		retrieveLastFives(state, lastFives) {
			state.lastFives = lastFives
		},
		retrieveToken(state, token) {
			state.token = token
		},
		retrieveUser(state, user) {
			state.user = user
		},
		updateLoading(state, loadingState) {
			state.loading = loadingState
		}
	},
	actions: {
		destroyToken(context) {
			axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
			// context.commit('updateLoading', true);
			if(context.getters.loggedIn) {
				return new Promise((resolve, reject) => {
					axios.post('/api/logout')
		                .then(response => {
		                	// context.commit('updateLoading', false)
		                	context.commit('deleteToken');
		                	context.commit('deleteUser');
		                	localStorage.removeItem('access_token')
		                    resolve(response);
		                })
		                .catch(error => {
		                	// context.commit('updateLoading', false);
		                	context.commit('deleteToken');
		                	context.commit('deleteUser');
		                	localStorage.removeItem('access_token')
		                	console.log(error);
		                	reject(error);
		                })
		        });
			}
		},

		retrieveLastFives(context) {
			// context.commit('updateLoading', true);
			axios.get('/api/stats/last_five')
                .then(response => {
                	// context.commit('updateLoading', false)
                    context.commit('retrieveLastFives', response.data)
                })
                .catch(error => {
                	// context.commit('updateLoading', false)
                	console.log(error)
                })
		},

		retrieveToken(context, credentials) {
			// context.commit('updateLoading', true);
			return new Promise((resolve, reject) => {
				axios.post('/api/login', {
					username: credentials.username,
					password: credentials.password
				})
	                .then(response => {
	                	// context.commit('updateLoading', false)
	                	context.commit('retrieveToken', response.data.access_token);
	                	localStorage.setItem('access_token', response.data.access_token)
	                    resolve(response);
	                })
	                .catch(error => {
	                	// context.commit('updateLoading', false)
	                	console.log(error);
	                	reject(error);
	                })
	        });
		},

		retrieveUserInfos(context) {
			axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
			// context.commit('updateLoading', true);
			if(context.getters.loggedIn) {
				return new Promise((resolve, reject) => {
					axios.get('/api/user')
		                .then(response => {
		                	// context.commit('updateLoading', false);
		                	context.commit('retrieveUser', response.data);
		                    resolve(response);
		                })
		                .catch(error => {
		                	// context.commit('updateLoading', false);
		                	console.log(error);
		                	reject(error);
		                })
		        });
			}
		},
		registerUser(context, data) {
			// context.commit('updateLoading', true);
			return new Promise((resolve, reject) => {
				axios.post('/api/register', data)
	                .then(response => {
	                	// context.commit('updateLoading', false);
	                    resolve(response);
	                })
	                .catch(error => {
	                	// context.commit('updateLoading', false)
	                	console.log(error);
	                	reject(error);
	                })
	        });
		},
		activateUser(context, data) {
			// context.commit('updateLoading', true);
			return new Promise((resolve, reject) => {
				axios.post('/api/activate', data)
	                .then(response => {
	                	// context.commit('updateLoading', false);
	                    resolve(response);
	                })
	                .catch(error => {
	                	// context.commit('updateLoading', false)
	                	console.log(error);
	                	reject(error);
	                })
	        });
		},


	}
})