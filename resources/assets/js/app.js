
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes.js';
import { store } from './store/store.js';

import LastFive from './components/LastFive.vue';
import Layout from './components/layout/Layout.vue';
import UserMenu from './components/menus/UserMenu.vue';
import VueMenu from './components/menus/Menu.vue';

Vue.use(VueRouter);

const router = new VueRouter({
	routes,
	mode: 'history'
});

export const eventBus = new Vue();

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.getters.loggedIn) {
      next({
        name: 'connexion'
      })
    } else {
      next()
    }
  } else if(to.matched.some(record => record.meta.requiresVisitor)) {
    // this route requires auth, check if logged in
    // if so, redirect to dashboard page.
    if (store.getters.loggedIn) {
      next({
        name: 'dashboard'
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

require('./bootstrap')

const app = new Vue({
    el: '#app',
    components: {
    	LastFive,
      Layout,
    	UserMenu,
    	VueMenu,
    },
    store: store,
    router: router
})
