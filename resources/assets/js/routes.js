import Index from './components/pages/Index';
import Login from './components/pages/Login';
import Logout from './components/pages/Logout';
import Register from './components/pages/Register';
import ActivateAccount from './components/pages/ActivateAccount';
import DashboardPanel from './components/pages/DashboardPanel';

import DashboardIndex from './components/pages/dashboard/Index';
import BalanceList from './components/pages/dashboard/BalanceList';
import ScheduleList from './components/pages/dashboard/ScheduleList';
import GamesHistoryList from './components/pages/dashboard/GamesHistoryList';
import UsersList from './components/pages/dashboard/UsersList';
import RolesList from './components/pages/dashboard/RolesList';
import MenusList from './components/pages/dashboard/menus/MenusList';
import SettingsList from './components/pages/dashboard/SettingsList';
import PostsList from './components/pages/dashboard/posts/PostsList';
import PagesList from './components/pages/dashboard/pages/PagesList';
import CommentsList from './components/pages/dashboard/comments/CommentsList';
import LeaguesList from './components/pages/dashboard/leagues/LeaguesList';


const routes = [
	{ 
		path: '/',
		name: 'home',
		component: Index 
	},
	{ 
		path: '/login',
		name: 'connexion',
		component: Login,
		meta: {
			requiresVisitor: true
		}
	},
	{ 
		path: '/register',
		name: 'register',
		component: Register,
		meta: {
			requiresVisitor: true
		}
	},
	{ 
		path: '/logout',
		name: 'logout',
		component: Logout,
		meta: {
			requiresAuth: true
		}
	},
	{ 
		path: '/dashboard',
		component: DashboardPanel,
		meta: {
			requiresAuth: true
		},
		children: [
			{
				path:'/',
				name: 'dashboard',
				component: DashboardIndex
			},
			{
				path: 'transactions',
				component: BalanceList
			},
			{
				path: 'horaire',
				component: ScheduleList
			},
			{
				path: 'historique-matchs',
				component: GamesHistoryList
			},
			{
				path: 'users',
				component: UsersList
			},
			{
				path: 'roles',
				component: RolesList
			},
			{
				path: 'menus',
				component: MenusList
			},
			{
				path: 'settings',
				component: SettingsList
			},
			{
				path: 'posts',
				component: PostsList
			},
			{
				path: 'pages',
				component: PagesList
			},
			{
				path: 'comments',
				component: CommentsList
			},
			{
				path: 'leagues',
				component: LeaguesList
			}
		]
	},
	{ 
		path: '/reglements',
		name: 'reglements',
		component: Index 
	},
	{
		path: '/activation/:id/:code',
		name: 'activation',
		component: ActivateAccount
	}
	
];

export default routes