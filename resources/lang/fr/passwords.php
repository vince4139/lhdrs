<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Les mots de passes doivent contenir au moins 6 caratères et être identique à la confirmation',
    'reset' => 'Votre mot de passe a été réinitialisé!',
    'sent' => 'Nous vous avons envoyé un courriel de réinitialisation de votre mot de passe.',
    'token' => 'Ce code de réinitialisation est invalide.',
    'user' => "Nous ne trouvons pas d'utilisateur utilisant cette addresse courriel.",

];
