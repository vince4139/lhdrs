<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => "Cette combinaison Courriel / Mot de passe n'existe pas dans notre base de données",
    'throttle' => "Trop d'insuccès de connexion. Veuillez rééssayer dans :seconds secondes.",

];
