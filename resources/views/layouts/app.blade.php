<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'LHDRS') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

{{-- @if(setting('site.background_site'))
<body style="background-image: url(storage/{{ setting('site.background_site') }});">
@else --}}
<body>
{{-- @endif --}}
    <div id="app">
        <header>
            <div class="container clearfix">
                <h1 class="graduate"><router-link :to="{ name: 'home' }">{{ config('app.name', 'LHDRS') }}</router-link></h1>
                <vue-menu class="main-nav" :items="{{ $main_menu }}"></vue-menu>
                <user-menu></user-menu>
            </div>
        </header>
        <div class="content">
            <last-five></last-five>
            <layout></layout>
        </div>
        <footer>
            <div class="container">
                <div class="col-lg-3">
                    <a href="/"><img src="{{ url('storage/logo.png') }}" alt="LHDRS"></a>
                </div>
                <div class="col-lg-6">
                    <vue-menu class="footer-nav" :items="{{ $footer_menu }}"></vue-menu >
                    <hr>
                    <p class="copyright">©2012-2019 LHDRS - Tous droits réservés.</p>
                </div>
            </div>
        </footer>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
