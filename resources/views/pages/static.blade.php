@extends('layouts.app')

@section('content')

<div class="col-lg-8 main-col">
	<div class="blocks-list ign-padding">
		<div class="block">
			<h1>{{ $page->title }}</h1>
			{!! $page->body !!}
		</div>
	</div>
</div>

@endsection

{{-- @section('sidebar')

<div class="col-lg-4">

</div>

@endsection --}}