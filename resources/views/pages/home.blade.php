@extends('layouts.app')

@section('content')
<div class="col-lg-8 main-col">
    <latest-news></latest-news>
</div>
@endsection


@section('sidebar')
<div class="col-lg-4">
	<div class="pub-top-sidebar">
	    <a href="http://placeholder.com" target="_blank"><img src="https://via.placeholder.com/325x250?text=325x250+MPU" alt="pub carrée"></a>
	</div>

	<div class="pub-second-sidebar">
	    <a href="http://placeholder.com" target="_blank"><img src="https://via.placeholder.com/325x250?text=325x250+MPU" alt="pub carrée"></a>
	</div>
</div>
@endsection
