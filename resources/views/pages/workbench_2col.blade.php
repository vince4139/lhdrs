@extends('layouts.app')

@section('content')
<div class="col-lg-8 main-col">
    <latest-news></latest-news>
    <div class="blocks-list ign-padding">
        <div class="block">
            <h2>Exemple de texte dans la section contenu</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, reiciendis, libero. Fugiat obcaecati atque et similique. Aspernatur cupiditate perspiciatis molestiae consectetur minus voluptate repudiandae modi amet nemo reiciendis doloremque laborum quis voluptatum, illo, accusamus sequi ipsa ipsam id ipsum unde rem necessitatibus ea. Laudantium sit numquam, rem laborum iure autem.
            </p>
        </div>
        <div class="block">
            <stats-grid></stats-grid>
        </div>
    </div>
</div>
@endsection

@section('sidebar')
<div class="col-lg-4">
    <div class="pub-top-sidebar">
        <a href="http://placeholder.com" target="_blank"><img src="https://via.placeholder.com/325x250?text=325x250+MPU" alt="pub carrée"></a>
    </div>
    <div class="blocks-list">
        <div class="block">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias magni vel aliquid repellendus, maiores tempora perspiciatis libero, saepe temporibus, neque voluptate fugiat necessitatibus quasi ullam accusamus totam animi veritatis consequatur.
            </p>
        </div>
        <div class="block">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate facilis magnam soluta ratione odit tenetur ipsam sapiente, architecto harum perspiciatis et neque itaque natus cupiditate sint molestiae atque minima aut.            
            </p>
        </div>
    </div>
</div>
@endsection
