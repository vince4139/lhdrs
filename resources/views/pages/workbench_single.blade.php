@extends('layouts.app')

@section('content')
<div class="col-lg-12 main-col">
    {{-- <latest-news></latest-news> --}}
    <div class="blocks-list">
        <div class="block">
            <h2>Exemple de texte dans la section contenu</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, reiciendis, libero. Fugiat obcaecati atque et similique. Aspernatur cupiditate perspiciatis molestiae consectetur minus voluptate repudiandae modi amet nemo reiciendis doloremque laborum quis voluptatum, illo, accusamus sequi ipsa ipsam id ipsum unde rem necessitatibus ea. Laudantium sit numquam, rem laborum iure autem.
            </p>
        </div>
        <div class="block">
            <stats-grid></stats-grid>
        </div>
    </div>
</div>
@endsection

