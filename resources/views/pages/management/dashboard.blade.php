@extends('layouts.app')

@section('content')

<div class="col-lg-8 main-col">
	<div class="blocks-list ign-padding">
		<div class="block">
			<div class="block__header clearfix">
				<h1 class="pull-left">Dashboard</h1>
				{{-- <a href="{{ route('inscriptions')}}" class="btn btn-default pull-right">Inscription à une ligue</a> --}}
			</div>
			<div class="block__content">
				<dashboard-panel></dashboard-panel>
			</div>
		</div>
	</div>
</div>

@endsection
@section('sidebar')
<div class="col-lg-4">
    <div class="pub-top-sidebar">
        <a href="http://placeholder.com" target="_blank"><img src="https://via.placeholder.com/325x250?text=325x250+MPU" alt="pub carrée"></a>
    </div>

    <div class="pub-second-sidebar">
        <a href="http://placeholder.com" target="_blank"><img src="https://via.placeholder.com/325x250?text=325x250+MPU" alt="pub carrée"></a>
    </div>
</div>
@endsection
