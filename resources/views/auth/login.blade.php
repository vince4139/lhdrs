@extends('layouts.app')

@section('content')
<div class="col-lg-8">
    <div class="blocks-list ign-padding">
        <div class="block">
            <h1>Connexion</h1>

            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">Courriel</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Mot de passe</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Se souvenir de moi!
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Se connecter
                            </button>

                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                Mot de passe oublié?
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('sidebar')
<div class="col-lg-4">
    <div class="pub-top-sidebar">
        <a href="http://placeholder.com" target="_blank"><img src="https://via.placeholder.com/325x250?text=325x250+MPU" alt="pub carrée"></a>
    </div>

    <div class="pub-second-sidebar">
        <a href="http://placeholder.com" target="_blank"><img src="https://via.placeholder.com/325x250?text=325x250+MPU" alt="pub carrée"></a>
    </div>
</div>
@endsection
