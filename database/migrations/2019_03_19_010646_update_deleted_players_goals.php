<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateDeletedPlayersGoals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('goals')->where('goal', '=', 0)->where('assist_one', '!=', 0)->update(['goal' => NULL]);
        DB::table('goals')->where('goal', '=', 0)->where('assist_one', '=', 0)->where('assist_two', '!=', 0)->update(['goal' => NULL, 'assist_one' => NULL]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
