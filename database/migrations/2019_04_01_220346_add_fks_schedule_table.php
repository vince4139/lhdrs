<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFksScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedule', function($table) {
            $table->integer('home')->unsigned()->nullable($value=true)->change();
            $table->integer('away')->unsigned()->nullable($value=true)->change();

            $table->foreign('home', 'fk_schedule_home_team_id')
                  ->references('id')->on('teams')
                  ->onDelete('set null');

            $table->foreign('away', 'fk_schedule_away_team_id')
                  ->references('id')->on('teams')
                  ->onDelete('set null');

            $table->foreign('publicity_home', 'fk_schedule_publicity_home_team_id')
                  ->references('id')->on('publicity')
                  ->onDelete('set null');

            $table->foreign('publicity_away', 'fk_schedule_publicity_away_team_id')
                  ->references('id')->on('publicity')
                  ->onDelete('set null');

            $table->foreign('league_id', 'fk_schedule_league_id')
                  ->references('id')->on('leagues')
                  ->onDelete('set null');

            $table->foreign('season_id', 'fk_schedule_season_id')
                  ->references('id')->on('seasons')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('schedule', function($table) {
            $table->integer('home')->nullable($value=true)->change();
            $table->integer('away')->nullable($value=true)->change();

            $table->dropForeign('fk_schedule_home_team_id');

            $table->dropForeign('fk_schedule_away_team_id');

            $table->dropForeign('fk_schedule_publicity_home_team_id');

            $table->dropForeign('fk_schedule_publicity_away_team_id');

            $table->dropForeign('fk_schedule_league_id');

            $table->dropForeign('fk_schedule_season_id');
        });
    }
}
