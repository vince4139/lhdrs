<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFksOnGoalAndAssistsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goals', function($table) {
            $table->foreign('goal_player_id', 'fk_goal_player_id')
                  ->references('id')->on('players')
                  ->onDelete('set null');

            $table->foreign('assist_one_player_id', 'fk_assist_one_player_id')
                  ->references('id')->on('players')
                  ->onDelete('set null');

            $table->foreign('assist_two_player_id', 'fk_assist_two_player_id')
                  ->references('id')->on('players')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goals', function($table) {
            $table->dropForeign('fk_goal_player_id');
            $table->dropForeign('fk_assist_one_player_id');
            $table->dropForeign('fk_assist_two_player_id');
        });
    }
}
