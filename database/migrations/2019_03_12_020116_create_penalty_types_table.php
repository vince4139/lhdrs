<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\PenaltyType;

class CreatePenaltyTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penalty_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('penalty_name');
            $table->smallInteger('penalty_time');
        });

        $penalty_types = [
            [
                'penalty_name' => 'Accrocher',
                'penalty_time' => 2
            ],
            [
                'penalty_name' => 'Bâton élevé',
                'penalty_time' => 2
            ],
            [
                'penalty_name' => 'Donner de la bande',
                'penalty_time' => 2
            ],
            [
                'penalty_name' => 'Contact illégal',
                'penalty_time' => 2
            ],
            [
                'penalty_name' => 'Obstruction',
                'penalty_time' => 2
            ],
            [
                'penalty_name' => 'Plonger',
                'penalty_time' => 2
            ],
            [
                'penalty_name' => 'Retarder le match',
                'penalty_time' => 2
            ],
            [
                'penalty_name' => 'Retenir',
                'penalty_time' => 2
            ],
            [
                'penalty_name' => 'Trébucher',
                'penalty_time' => 2
            ],
            [
                'penalty_name' => 'Trop de joueur sur la glace',
                'penalty_time' => 2
            ],
            [
                'penalty_name' => 'Antisportif - Mineure',
                'penalty_time' => 2
            ],
            [
                'penalty_name' => 'Antisportif - Mineure Double',
                'penalty_time' => 4
            ],
            [
                'penalty_name' => 'Antisportif - Majeure',
                'penalty_time' => 5
            ],
            [
                'penalty_name' => 'Antisportif - Inconduite',
                'penalty_time' => 10
            ],
            [
                'penalty_name' => 'Coup de bâton - Mineure',
                'penalty_time' => 2
            ],
            [
                'penalty_name' => 'Coup de bâton - Mineure Double',
                'penalty_time' => 4
            ],
            [
                'penalty_name' => 'Coup de bâton - Majeure',
                'penalty_time' => 5
            ],
            [
                'penalty_name' => 'Coup de bâton - Inconduite',
                'penalty_time' => 10
            ],
            [
                'penalty_name' => 'Donner de la bande - Mineure',
                'penalty_time' => 2
            ],
            [
                'penalty_name' => 'Donner de la bande - Mineure Double',
                'penalty_time' => 4
            ],
            [
                'penalty_name' => 'Donner de la bande - Majeure',
                'penalty_time' => 5
            ],
            [
                'penalty_name' => 'Donner de la bande - Inconduite',
                'penalty_time' => 10
            ],
            [
                'penalty_name' => 'Coup de coude',
                'penalty_time' => 4
            ],
            [
                'penalty_name' => 'Coup de coude - Majeure',
                'penalty_time' => 5
            ],
            [
                'penalty_name' => 'Coup de coude - Inconduite',
                'penalty_time' => 10
            ],
            [
                'penalty_name' => 'Coup de genou',
                'penalty_time' => 4
            ],
            [
                'penalty_name' => 'Coup de genou - Majeure',
                'penalty_time' => 5
            ],
            [
                'penalty_name' => 'Coup de genou - Inconduite',
                'penalty_time' => 10
            ],
            [
                'penalty_name' => 'Dardage',
                'penalty_time' => 4
            ],
            [
                'penalty_name' => 'Dardage - Majeure',
                'penalty_time' => 5
            ],
            [
                'penalty_name' => 'Dardage - Inconduite',
                'penalty_time' => 10
            ],
            [
                'penalty_name' => 'Double échec',
                'penalty_time' => 4
            ],
            [
                'penalty_name' => 'Double échec - Majeure',
                'penalty_time' => 5
            ],
            [
                'penalty_name' => 'Double échec - Inconduite',
                'penalty_time' => 10
            ],
            [
                'penalty_name' => 'Rudesse',
                'penalty_time' => 4
            ],
            [
                'penalty_name' => 'Rudesse - Majeure',
                'penalty_time' => 5
            ],
            [
                'penalty_name' => 'Rudesse - Inconduite',
                'penalty_time' => 10
            ],
            [
                'penalty_name' => 'Charger - Majeure + Inconduite',
                'penalty_time' => 15
            ],
            [
                'penalty_name' => 'Bataille - Majeure + Inconduite',
                'penalty_time' => 15
            ],
            [
                'penalty_name' => 'Tentative de blesser - Majeure + Inconduite',
                'penalty_time' => 15
            ],
            [
                'penalty_name' => 'Mise en échec - Majeure + Inconduite',
                'penalty_time' => 15
            ],
            [
                'penalty_name' => '6 minutes de punitions - Majeure + Inconduite',
                'penalty_time' => 15
            ],
        ];

        foreach ($penalty_types as $pt) {
            $penalty_type = new PenaltyType;

            $penalty_type->penalty_name = $pt['penalty_name'];    
            $penalty_type->penalty_time = $pt['penalty_time']; 

            $penalty_type->save();   
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penalty_types');
    }
}
