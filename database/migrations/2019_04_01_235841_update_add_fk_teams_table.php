<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAddFkTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teams', function($table) {
            $table->integer('league_id')->nullable($value=true)->unsigned()->change();

            $table->foreign('league_id', 'fk_teams_league_id')
                  ->references('id')->on('leagues')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teams', function($table) {
            $table->integer('league_id')->unsigned()->change();

            $table->dropForeign('fk_teams_league_id');
        });
    }
}
