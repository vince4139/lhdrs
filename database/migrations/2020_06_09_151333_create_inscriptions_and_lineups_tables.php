<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscriptionsAndLineupsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('inscriptions_test')) {
            Schema::create('inscriptions_test', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();
                $table->integer('player_id')->unsigned();
                $table->integer('season_id')->unsigned();
                $table->tinyInteger('is_goalie');
                $table->timestamps();

                $table->foreign('player_id', 'fk_player_subscribed')->references('id')->on('players')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('season_id', 'fk_season_subscribed')->references('id')->on('seasons')->onDelete('cascade')->onUpdate('cascade');

                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->change();
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->change();
            });
        }
        if(!Schema::hasTable('lineups_test')) {
            Schema::create('lineups_test', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();
                $table->integer('player_id')->unsigned();
                $table->bigInteger('schedule_id')->unsigned();
                $table->tinyInteger('is_goalie');
                $table->tinyInteger('is_substitute');
                $table->string('player_number')->nullable(true);
                $table->tinyInteger('is_home');
                $table->timestamps();

                $table->foreign('player_id', 'fk_player_ingame')->references('id')->on('players')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('schedule_id', 'fk_game_ingame')->references('id')->on('schedule')->onDelete('cascade')->onUpdate('cascade');

                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->change();
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->change();
            });
        }

        if(!Schema::hasTable('teams_lineup_test')) {
            Schema::create('teams_lineup_test', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();
                $table->integer('player_id')->unsigned();
                $table->integer('team_id')->unsigned();
                $table->tinyInteger('is_goalie');
                $table->string('player_number')->nullable(true);
                $table->timestamps();

                $table->foreign('player_id', 'fk_player_team')->references('id')->on('players')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('team_id', 'fk_team_player')->references('id')->on('teams')->onDelete('cascade')->onUpdate('cascade');

                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->change();
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscriptions');
        Schema::dropIfExists('lineups');
    }
}
