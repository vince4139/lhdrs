<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFksToPenaltysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('penaltys', function($table) {
            $table->bigInteger('game_id')->unsigned()->nullable($value=true)->change();
            $table->integer('player')->unsigned()->nullable($value=true)->change();
            $table->integer('penalty_type')->unsigned()->nullable($value=true)->change();
        });

        Schema::table('penaltys', function($table) {
            $table->foreign('game_id', 'fk_penaltys_game_id')
                  ->references('id')->on('schedule')
                  ->onUpdate('cascade')
                  ->onDelete('set null');

            $table->foreign('player', 'fk_penaltys_player_id')
                  ->references('id')->on('players')
                  ->onDelete('set null');

            $table->foreign('penalty_type', 'fk_penaltys_penalty_type_id')
                  ->references('id')->on('penalty_types')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penaltys', function($table) {
            $table->dropForeign('fk_penaltys_game_id');
            $table->dropForeign('fk_penaltys_player_id');
            $table->dropForeign('fk_penaltys_penalty_type_id');
        });
    }
}
