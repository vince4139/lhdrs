<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\League;
use App\Season;

class UpdateForSimplerLeagueWorkflow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $leagues = League::all();

        Schema::table('seasons', function (Blueprint $table) {
            $table->integer('type')->unsigned()->default(NULL);
            $table->date('end')->nullable();
            // $table->string('game_start', 12)->nullable(true)->default('NULL');
            // $table->string('game_end', 12)->nullable(true)->default('NULL');
            $table->json('games_per_week')->nullable(true)->default(NULL);
            $table->integer('arena_id')->unsigned()->nullable()->default(NULL);
            $table->string('notes')->nullable(true)->default('NULL');
            $table->string('level')->nullable(true)->default('NULL');
            $table->integer('publicity_id_home')->nullable(true)->default(NULL);
            $table->integer('publicity_id_away')->nullable(true)->default(NULL);
            $table->decimal('player_season_price')->nullable(true)->default(NULL);
            $table->decimal('goalie_season_price')->nullable(true)->default(NULL);
            $table->decimal('player_game_price')->nullable(true)->default(NULL);
            $table->decimal('goalie_game_price')->nullable(true)->default(NULL);
            $table->integer('games_number')->nullable(true)->default(NULL);
            $table->string('players')->nullable(true)->default('NULL');
            $table->string('goalies')->nullable(true)->default('NULL');
            $table->integer('total_spots')->unsigned()->nullable(true);
            $table->integer('total_spots_goalies')->unsigned()->nullable(true);
            $table->integer('total_spots_teams')->unsigned()->nullable(true);
            $table->tinyInteger('inscription_players')->nullable(true)->default(1);
            $table->tinyInteger('inscription_goalies')->nullable(true)->default(1);
            $table->tinyInteger('find_match')->nullable(true)->default(1);
            $table->integer('order')->nullable(true)->default(NULL);
            $table->integer('status')->nullable(true)->default(NULL);
        });

        foreach ($leagues as $league) {
            $season = Season::find($league->season_id);

            if($league->game_start == null) {

                if($league->id == 6 || $league->id == 7)
                {
                    $start = '21:00';
                    $end = '00:15';
                } else {
                    $stripped = str_replace(' ', '', $league->game_hour);

                    $start = substr($stripped, 0, 5);
                    $end = substr($stripped, 6, 5);
                }
                // $season->game_start = ($start === '' || $start === NULL) ? NULL : $start;
                // $season->game_end = ($end === '' || $end === NULL || $end === 0) ? NULL : $end;
            } 
            else {
                // $season->game_start = $league->game_start;
                // $season->game_end = $league->game_end;
                $start = $league->game_start;
                $end = $league->game_end;
            }

            $arr = [
                "game_1" => [
                    "day" => $league->day,
                    "start" => $start,
                    "end" => $end,
                ]
            ];

            $season->games_per_week = json_encode($arr, 0, 2);

            // var_dump(json_encode($arr));

            if($league->status == 1) { // saison active
                $org_number = 3000;
            } else if($league->status == 2) { // saison inactive
                $org_number = 1000;
            } else { // saison no débutée
                $org_number = 2000;
            }

            $season->type = $league->type;
            $season->arena_id = $league->arena;
            $season->notes = $league->notes;
            $season->level = $league->level;
            $season->publicity_id_home = $league->publicity_home;
            $season->publicity_id_away = $league->publicity_away;
            $season->player_season_price = $league->player_season_price;
            $season->goalie_season_price = $league->goalie_season_price;
            $season->player_game_price = $league->player_game_price;
            $season->goalie_game_price = $league->goalie_game_price;
            $season->games_number = $league->games_number;
            $season->players = $league->players;
            $season->goalies = $league->goalie;
            $season->total_spots = null;
            $season->total_spots_goalies = null;
            $season->total_spots_teams = null;
            $season->inscription_players = $league->inscription_players;
            $season->inscription_goalies = $league->inscription_goalies;
            $season->find_match = $league->find_match;
            $season->order = $league->leagues_order + $org_number;
            $season->status = $league->status;

            $season->save();
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
