<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\LeagueStatus;
use App\SeasonType;

class CreateSeasonTypeAndLeagueStatusTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $types = [
            [
                'name' => 'Amicale',
            ],
            [
                'name' => 'Compétitive',
            ],
        ];

        Schema::create('season_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        foreach ($types as $t) {
            $type = new SeasonType;

            $type->name = $t['name'];

            $type->save();
        }

        $statuses = [
            [
                'name' => 'Débutée',
            ],
            [
                'name' => 'Inactive',
            ],
            [
                'name' => 'Non débutée',
            ],
        ];

        Schema::create('league_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        foreach ($statuses as $s) {
            $status = new LeagueStatus;

            $status->name = $s['name'];

            $status->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('season_types');
        Schema::dropIfExists('league_status');
    }
}
