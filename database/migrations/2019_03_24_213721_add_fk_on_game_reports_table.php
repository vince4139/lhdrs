<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkOnGameReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_reports', function($table) {
            $table->bigInteger('game_id')->nullable($value=true)->unsigned()->change();

            $table->foreign('game_id', 'fk_game_id_2')
                  ->references('id')->on('schedule')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function($table) {
            $table->integer('game_id')->unsigned()->change();
            $table->dropForeign('fk_game_id_2');
        });
    }
}
