<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ColumnsTypeUpdate2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('players_stats', function($table) {
            $table->unsignedInteger('id')->change();
            $table->unsignedInteger('player_id')->change();
            $table->unsignedInteger('league_id')->change();
            $table->unsignedInteger('team_id')->change();
            $table->unsignedInteger('season_id')->change();
            $table->unsignedSmallInteger('gp')->change();
            $table->unsignedSmallInteger('goals')->change();
            $table->unsignedSmallInteger('assists')->change();
            $table->unsignedSmallInteger('goalie_shotout')->change();
            $table->unsignedSmallInteger('goalie_goals_against')->change();
            $table->unsignedSmallInteger('goalie_saves')->change();
            $table->unsignedSmallInteger('goalie_penaltys')->change();
            $table->unsignedSmallInteger('wins')->change();
            $table->unsignedSmallInteger('losses')->change();
            $table->unsignedSmallInteger('ties')->change();
            $table->unsignedSmallInteger('overtime_loss')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('players_stats', function($table) {
            $table->integer('id')->change();
            $table->integer('player_id')->change();
            $table->integer('league_id')->change();
            $table->integer('team_id')->change();
            $table->string('season_id')->change();
            $table->integer('gp')->change();
            $table->integer('goals')->change();
            $table->integer('assists')->change();
            $table->integer('goalie_shotout')->change();
            $table->integer('goalie_goals_against')->change();
            $table->integer('goalie_saves')->change();
            $table->integer('goalie_penaltys')->change();
            $table->integer('wins')->change();
            $table->integer('losses')->change();
            $table->integer('ties')->change();
            $table->integer('overtime_loss')->change();
        });
    }
}
