<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddFksPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function($table) {
            DB::statement("ALTER TABLE posts ALTER status SET DEFAULT 'DRAFT'");
            
            $table->foreign('league_id', 'fk_posts_league_id')
                  ->references('id')->on('leagues')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function($table) {
            $table->integer('status')->nullable($value=true)->change();

            $table->dropForeign('fk_posts_league_id');
            $table->dropForeign('fk_posts_status_id');
                  
        });
    }
}
