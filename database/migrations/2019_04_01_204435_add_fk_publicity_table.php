<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkPublicityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('publicity', function($table) {
            $table->integer('format')->nullable($value=true)->unsigned()->change();
            $table->foreign('format', 'fk_publicity_format_id')
                  ->references('id')->on('publicity_formats')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publicity', function($table) {
            $table->dropForeign('fk_publicity_format_id');
        });
    }
}
