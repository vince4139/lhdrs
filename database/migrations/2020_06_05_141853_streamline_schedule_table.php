<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Game;

class StreamlineScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $names_to_update = [
            [
                'value' => 'Arena Olivier Ford',
                'replacement' => 105
            ],
            [
                'value' => 'Arena Olivier Ford St-Hubert',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif Gaétan-Boucher',
                'replacement' => 105
            ],
            [
                'value' => 'Centre sportif Gaétan-Boucher',
                'replacement' => 105
            ],
            [
                'value' => 'Centre sportif GaÃ©tan-Boucher',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif Gaétan-Boucher (St-Hubert)',
                'replacement' => 105
            ],
            [
                'value' => 'Centre sportif Gaétan-Boucher (St-Hubert)',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif Gaétan-Boucher (1 HEURE DE RÉSERVAT',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif Gaétan-Boucher St-Hubert - Compétitif LHDRS#3',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif Gaétan-Boucher (1 HEURE DE RÉSERVATION : PRATIQUE + MATCH VS CENTORS)',
                'replacement' => 105
            ],
            [
                'value' => ' Centre sportif Gaétan-Boucher (St-Hubert)',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif Gaétan-Boucher (HOCKEY LIBRE)',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif Gaétan-Boucher - Compétitif D2 - LHDRS#3',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif Gaétan-Boucher - Compétitif D1 - LHDRS#3',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif Gaetan-Boucher',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif Gaetan-Boucher (compétitif "amical")',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif Gaétan-Boucher (GRATUIT POUR LES FEMMES)',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif de La Prairie',
                'replacement' => 101
            ], 
            [
                'value' => 'Centre Sportif De La Prairie',
                'replacement' => 101
            ],
            [
                'value' => 'Centre sportif de la Prairie',
                'replacement' => 101
            ], 
            [
                'value' => 'Complexe Sportif Longueuil',
                'replacement' => 102
            ], 
            [
                'value' => 'Complexe sportif Longueil',
                'replacement' => 102
            ],
            [
                'value' => ' Complexe sportif Longueil',
                'replacement' => 102
            ],
            [
                'value' => ' Complexe sportif Longueuil',
                'replacement' => 102
            ], 
            [
                'value' => 'Complexe sportif Longueuil',
                'replacement' => 102
            ],
            [
                'value' => 'Complexe Sportif Longueuil (compétitif "amical")',
                'replacement' => 102
            ],   
            [
                'value' => 'Patinoire exterieur "COUVERTE" de McMasterville',
                'replacement' => 103
            ],
            [
                'value' => 'Centre Sportif Gaétan-Boucher (1 HEURE DE RÃ?SERVATION)',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif Gaétan-Boucher (1 HEURE DE RÉSERVATION)',
                'replacement' => 105
            ],
            [
                'value' => 'Complexe Sportif Bell',
                'replacement' => 99
            ],
            [
                'value' => 'Complexe sportif Bell',
                'replacement' => 99
            ],
            [
                'value' => 'Centre sportif Bell',
                'replacement' => 99
            ],
            [
                'value' => 'Complexe Sportif Bell de Brossard',
                'replacement' => 99
            ],
            [
                'value' => 'Complexe Sportif Bell (compétitif "amical")',
                'replacement' => 99
            ],
            [
                'value' => 'Complexe sportif Bell (Brossard)',
                'replacement' => 99
            ],
            [
                'value' => ' Complexe sportif Bell (Brossard)',
                'replacement' => 99
            ],
            [
                'value' => 'Complexe sportif Bell (Brossard) - Compétitif LHDRS#3',
                'replacement' => 99
            ],
            [
                'value' => 'Complexe Sportif Bell (HOCKEY LIBRE)',
                'replacement' => 99
            ],
            [
                'value' => 'Complexe Sportif Sportscene Mont-Saint-Hilaire',
                'replacement' => 120
            ],
            [
                'value' => 'Complexe récréatif Gadbois',
                'replacement' => 121
            ],
            [
                'value' => '4 Glaces',
                'replacement' => 100
            ],
            [
                'value' => '4 Glaces (Brossard)',
                'replacement' => 100
            ],
            [
                'value' => '4 Glaces (Brossard)',
                'replacement' => 100
            ],
            [
                'value' => '4 glaces de Brossard',
                'replacement' => 100
            ],
            [
                'value' => '4 Glaces de Brossard - 10$ joueur et 5$ gardien',
                'replacement' => 100
            ],
            [
                'value' => ' Les 4 Glaces',
                'replacement' => 100
            ],
            [
                'value' => '4 glaces de Brossard - de 12:00 à 13:15',
                'replacement' => 100
            ],
            [
                'value' => '4 Glaces de Brossard',
                'replacement' => 100
            ],
            [
                'value' => 'Centre Gilles-Chabot',
                'replacement' => 98
            ],
            [
                'value' => 'Centre Gilles-Chabot (compétitif "amical")',
                'replacement' => 98
            ],
            [
                'value' => 'Aréna du CCS Ste-Julie',
                'replacement' => 106
            ],
            [
                'value' => 'Centre Sportif Duval Auto Boucherville',
                'replacement' => 98
            ],
            [
                'value' => 'Centre Vidéotron',
                'replacement' => 111
            ],
            [
                'value' => ' ',
                'replacement' => NULL
            ],
            [
                'value' => 'Complexe Sportif Duval Auto',
                'replacement' => 98
            ],
            [
                'value' => 'Isatis Sport Chambly',
                'replacement' => 119
            ],
            [
                'value' => 'Isatis Sport Chambly - Compétitif D1 TEAM CANADA',
                'replacement' => 119
            ],
            [
                'value' => 'Isatis Sport Chambly - Compétitif D2 - AED.com et Sharks',
                'replacement' => 119
            ],
            [
                'value' => 'Rosanne-Laflamme (St-Hubert)',
                'replacement' => 122
            ],
            [
                'value' => 'Centre Bell',
                'replacement' => 109
            ],
            [
                'value' => "Glaces de l'Est ( 3 VS 3 )",
                'replacement' => 110
            ],
            [
                'value' => "Glaces de l'Est",
                'replacement' => 110
            ],
            [
                'value' => "Isatis Sport St-Constant",
                'replacement' => 113
            ],
            [
                'value' => "Aréna municipal de la ville de La Prairie",
                'replacement' => 117
            ],
        ];

        $games=Game::all();

        Schema::table('schedule', function($table) {
            $table->string('arena', 244)->nullable($value=true)->change();
        });

        foreach ($games as $game) {
            foreach ($names_to_update as $ntu) {
                if($game->arena === $ntu['value']) {
                    $game->arena = $ntu['replacement'];
                }
            }

            if($game->id == 1378 || $game->id == 1379 || $game->id == 1380) {
                $game->game_start = '12:00';
                $game->game_end = '13:15';
            }

            $game->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
