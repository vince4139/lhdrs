<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

use App\User;

class PopulateRoleIdInUsersWithUserRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $uroles = DB::table('user_roles')->select('user_id')->get();

        foreach ($uroles as $urole) {
            $user = User::find($urole->user_id);

            $user->save();
        }

        // Run DataTypesTableSeeder, PermissionsTableSeeder, PermissionRoleTableSeeder;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
