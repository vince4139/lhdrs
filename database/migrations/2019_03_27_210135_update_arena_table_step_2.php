<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateArenaTableStep2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arenas', function($table) {
            if(Schema::hasColumn('arenas', 'league_name') && Schema::hasColumn('arenas', 'day')) {
                $table->dropColumn(['league_name', 'day']);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arenas', function($table) {
            $table->string('league_name', 255)->nullable($value=true);
            $table->string('day', 20)->nullable($value=true);

            $table->bigInteger('id')->change();
        });
    }
}
