<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameEntryIdToPostIdComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('comments', 'entry_id') && !Schema::hasColumn('comments', 'post_id')) {
            Schema::table('comments', function($table) {
                $table->renameColumn('entry_id', 'post_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function($table) {
            $table->renameColumn('post_id', 'entry_id');
        });
    }
}
