<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class UpdateLeagueTableStep3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('leagues', function($table) {
            $table->integer('arena')->unsigned()->nullable($value=true)->change();

            $table->foreign('arena', 'fk_arena_id')
                  ->references('id')->on('arenas')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leagues', function($table) {
            $table->dropForeign('fk_arena_id');

            $table->string('arena', 244);
        });
    }
}
