<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\League;

class UpdateLeaguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $leagues = League::all();

        // $leagues_without_order = [58,59,74];

        $names_to_update = [
            [
                'value' => 'Centre Sportif Gaétan-Boucher',
                'replacement' => 105
            ],
            [
                'value' => 'Centre sportif Gaétan-Boucher',
                'replacement' => 105
            ],
            [
                'value' => 'Centre sportif GaÃ©tan-Boucher',
                'replacement' => 105
            ], 
            [
                'value' => 'Centre Sportif de La Prairie',
                'replacement' => 101
            ], 
            [
                'value' => 'Centre Sportif De La Prairie',
                'replacement' => 101
            ], 
            [
                'value' => 'Complexe Sportif Longueuil',
                'replacement' => 102
            ], 
            [
                'value' => 'Complexe sportif Longueil',
                'replacement' => 102
            ],
            [
                'value' => ' Complexe sportif Longueil',
                'replacement' => 102
            ],
            [
                'value' => ' Complexe sportif Longueuil',
                'replacement' => 102
            ],   
            [
                'value' => 'Patinoire exterieur "COUVERTE" de McMasterville',
                'replacement' => 103
            ],
            [
                'value' => 'Centre Sportif Gaétan-Boucher (1 HEURE DE RÃ?SERVATION)',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif Gaétan-Boucher (1 HEURE DE RÉSERVATION)',
                'replacement' => 105
            ],
            [
                'value' => 'Centre Sportif Gaétan-Boucher (St-Hubert)',
                'replacement' => 105
            ],
            [
                'value' => 'Complexe Sportif Bell',
                'replacement' => 99
            ],
            [
                'value' => 'Complexe Sportif Bell (compétitif "amical")',
                'replacement' => 99
            ],
            [
                'value' => 'Complexe Sportif Sportscene Mont-Saint-Hilaire',
                'replacement' => 120
            ],
            [
                'value' => 'Complexe récréatif Gadbois',
                'replacement' => 121
            ],
            [
                'value' => '4 Glaces',
                'replacement' => 100
            ],
            [
                'value' => '4 glaces de Brossard',
                'replacement' => 100
            ],
            [
                'value' => '4 Glaces de Brossard',
                'replacement' => 100
            ],
            [
                'value' => ' Les 4 Glaces',
                'replacement' => 100
            ],
            [
                'value' => 'Centre Gilles-Chabot',
                'replacement' => 98
            ],
            [
                'value' => 'Aréna du CCS Ste-Julie',
                'replacement' => 106
            ],
            [
                'value' => 'Centre Sportif Duval Auto Boucherville',
                'replacement' => 98
            ],
            [
                'value' => 'Centre Vidéotron',
                'replacement' => 111
            ],
            [
                'value' => ' ',
                'replacement' => NULL
            ],
            [
                'value' => 'Complexe Sportif Duval Auto',
                'replacement' => 98
            ],


        ];

        Schema::table('leagues', function($table) {
            $table->string('arena', 244)->nullable($value=true)->change();
        });

        foreach ($leagues as $l) {
            $l->publicity_home = NULL;
            $l->publicity_away = NULL;

            if($l->leagues_order == 0) {
                $l->leagues_order = NULL;
            }

            foreach ($names_to_update as $ntu) {
                if($l->arena === $ntu['value']) {
                    $l->arena = $ntu['replacement'];
                }
            }

            $l->save();
        
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
