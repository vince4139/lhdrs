<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Game;

class UpdateScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $games = App\Game::all();

        foreach ($games as $game) {

            if($game->publicity_home == 0 && $game->publicity_away == 0) {
                
                $game->publicity_home = NULL;
                $game->publicity_away = NULL;

                $game->save();
            }
        }

        Schema::table('schedule', function($table) {
            $table->integer('league_id')->nullable()->unsigned()->change();
            $table->integer('season_id')->nullable()->unsigned()->change();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedule', function($table) {
            $table->integer('league_id')->unsigned()->change();
            $table->integer('season_id')->unsigned()->change();
        });
    }
}
