<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Gamesheet;

class UpdateGamesheetsTableForFkProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('gamesheets', function($table) {
            $table->integer('user_id')->nullable($value=true)->unsigned()->change();
        });

        $gamesheet = App\Gamesheet::find(2027);

        $gamesheet->user_id = NULL;

        $gamesheet->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gamesheets', function($table) {
            $table->integer('user_id')->unsigned()->change();
        });
    }
}
