<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateParametersTableToArenaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('arenas') && Schema::hasTable('parameters')) {
            Schema::rename('parameters', 'arenas');

            Schema::table('arenas', function($table) {
                $table->increments('id')->change();
                $table->renameColumn('arena', 'name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('arenas') && !Schema::hasTable('parameters')) {
            Schema::rename('arenas', 'parameters');
        }
    }
}
