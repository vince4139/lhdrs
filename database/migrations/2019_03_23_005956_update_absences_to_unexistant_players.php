<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Absence;

class UpdateAbsencesToUnexistantPlayers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $abs_to_update = [3563, 4703, 7799, 13588];

        foreach ($abs_to_update as $atu) {
            $abs = App\Absence::find($atu);

            $abs->player_id = NULL;

            $abs->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
