<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\League;
use App\Arena;

class UpdateLeagueTableStep2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $arenas = App\Arena::all();
        $leagues = App\League::all();


        foreach ($leagues as $l) {
            foreach ($arenas as $arena) {
                if($l->arena == $arena->name && $arena->name != NULL) {
                    $l->arena = $arena->id;
                } else if(empty($l->arena)) {
                    $l->arena = NULL;
                }
            }
            $l->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
