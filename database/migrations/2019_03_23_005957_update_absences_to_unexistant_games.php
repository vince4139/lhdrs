<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Absence;

class UpdateAbsencesToUnexistantGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('absences', function($table){
            $table->bigInteger('game_id')->nullable($value=true)->unsigned()->change();
        });

        $absences = Absence::all();

        foreach ($absences as $a) {

            if($a->game == null) {
                $a->game_id = NULL;
                $a->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('absences', function($table){
            $table->integer('game_id')->unsigned()->change();
        });
    }
}
