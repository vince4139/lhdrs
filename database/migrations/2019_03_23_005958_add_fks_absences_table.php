<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFksAbsencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('absences', function($table) {
            $table->foreign('player_id', 'fk_player_id')
                  ->references('id')->on('players')
                  ->onDelete('set null');

            $table->foreign('game_id', 'fk_game_id')
                  ->references('id')->on('schedule')
                  ->onUpdate('cascade')
                  ->onDelete('set null');

            $table->foreign('sub_id', 'fk_sub_id')
                  ->references('id')->on('substitution')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('absences', function($table) {
            $table->dropForeign('fk_player_id');
            $table->dropForeign('fk_game_id');
            $table->dropForeign('fk_sub_id');
        });
    }
}
