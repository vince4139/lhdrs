<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

use App\Player;
use App\Team;

class FillTeamsLineupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $teams = Team::all();

        foreach ($teams as $team) {
            $players = explode(',', $team->players);
            $goalie_id = $team->goalie;

            foreach ($players as $player_id) {

                // check if player exists
                $player = Player::find($player_id);
                if($player != null) {
                    DB::table('teams_lineup_test')->insert([
                        "player_id" => $player_id,
                        "team_id" => $team->id,
                        "is_goalie" => 0
                    ]);
                }
            }

            // check if player exists
            $goalie = Player::find($goalie_id);
            
            if($goalie != null) {
                DB::table('teams_lineup_test')->insert([
                    "player_id" => $goalie_id,
                    "team_id" => $team->id,
                    "is_goalie" => 1
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
