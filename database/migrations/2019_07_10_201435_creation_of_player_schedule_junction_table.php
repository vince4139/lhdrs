<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreationOfPlayerScheduleJunctionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('player_schedule', function($table){
        //     $table->integer('game_id')->unsigned();
        //     $table->integer('player_id')->unsigned();
        //     $table->tinyInteger('is_home')->unsigned()->default(0);
        //     $table->tinyInteger('is_sub')->unsigned()->default(0);
        //     $table->tinyInteger('is_goalie')->unsigned()->default(0);

        //     $table->foreign('game_id', 'fk_game_id')
        //           ->references('id')->on('schedule')
        //           ->onDelete('cascade');

        //     $table->foreign('player_id', 'fk_player_id')
        //           ->references('id')->on('players')
        //           ->onDelete('cascade');
        // });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropTable('player_schedule');
    }
}
