<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Penalty;
use App\Player;

class UpdatePenaltysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('penaltys', function($table) {
            $table->bigInteger('game_id')->nullable($value=true)->unsigned()->change();
            $table->integer('player')->nullable($value=true)->unsigned()->change();
            $table->integer('penalty_type')->nullable($value=true)->unsigned()->change();
        });


        $penalties_to_delete = App\Penalty::destroy(4060);
        $penalties = App\Penalty::all();

        foreach ($penalties as $penalty) {
            $player = App\Player::find($penalty->player);

            if($player == null) {
                $penalty->player = NULL;
            }

            if($penalty->penalty_type == 0) {
                $penalty->penalty_type = 11;
            }

            $penalty->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
