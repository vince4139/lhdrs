<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Player;

class UpdateAddFkUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //$invalid_users_player = [19,397,1206,3201,3357,4086,4105];

        // foreach ($invalid_users_player as $iup) {
        //     DB::table('users')->where('id', $iup)
        //                       ->update(['player_id' => NULL]);
        // }

        $users = App\User::all();

        foreach ($users as $u) {
            $player = App\Player::where('id', '=', $u->player_id)->first();
            if($player === null) {
                $u->player_id = null;
                $u->save();
            }
        }

        Schema::table('users', function($table) {
            $table->foreign('player_id', 'fk_users_player_id')
                  ->references('id')->on('players')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropForeign('fk_users_player_id');
        });
    }
}
