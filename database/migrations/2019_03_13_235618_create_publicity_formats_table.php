<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\PublicityFormat;

class CreatePublicityFormatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicity_formats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->smallInteger('width');
            $table->smallInteger('height');
        });

        $formats = [
            [
                'name' => 'Big Box',
                'width' => 300,
                'height' => 250
            ],
            [
                'name' => 'Leaderboard',
                'width' => 728,
                'height' => 90
            ]
        ];

        foreach ($formats as $f) {
            $format = new PublicityFormat;

            $format->name = $f['name'];
            $format->width = $f['width'];
            $format->height = $f['height'];

            $format->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publicity_formats');
    }
}
