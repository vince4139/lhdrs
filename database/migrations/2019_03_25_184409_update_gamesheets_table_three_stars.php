<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Gamesheet;

class UpdateGamesheetsTableThreeStars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $sheets = App\Gamesheet::all();
        

        foreach ($sheets as $sheet) {

            if($sheet->star_three == 0) {
                $sheet->star_three = NULL;
            }

            if($sheet->star_two == 0) {
                $sheet->star_two = NULL;
            }

            if($sheet->star_one == 0) {
                $sheet->star_one = NULL;
            }

            $sheet->save();
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
