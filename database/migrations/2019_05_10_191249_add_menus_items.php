<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Menu;
use App\MenuItem;

class AddMenusItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(!Schema::hasTable('menu_items')) {
            Schema::create('menu_items', function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->integer('menu_id')->unsigned();
                $table->string('title');
                $table->string('url');
                $table->string('icon_class')->nullable();
                $table->integer('parent_id')->unsigned();
                $table->integer('order')->unsigned()->nullable(false);
                $table->timestamp('created_at')->default(NULL);
                $table->timestamp('updated_at')->default(NULL);
            });
        }


        $menus_db = Menu::all();

        $menus = [
            'admin' => [
                [
                    'menu_id' => 1,
                    'title' => 'Dashboard',
                    'url' => '/dashboard',
                    'icon_class'=> 'glyphicon-th-large',
                    'parent_id'=> NULL,
                    'order'=> 1
                ],
                [
                    'menu_id' => 1,
                    'title' => 'Utilisateurs',
                    'url'=> '/dashboard/users',
                    'icon_class'=> 'glyphicon-user',
                    'parent_id'=> NULL,
                    'order'=> 2
                ],
                [
                    'menu_id' => 1,
                    'title' => 'Ligues',
                    'url'=> '/dashboard/leagues',
                    'icon_class'=> 'glyphicon-calendar',
                    'parent_id'=> NULL,
                    'order'=> 3
                ],
                [
                    'menu_id' => 1,
                    'title' => 'Créateur de menus',
                    'url'=> '/dashboard/menus',
                    'icon_class'=> 'glyphicon-list-alt',
                    'parent_id'=> NULL,
                    'order'=> 4
                ],
                [
                    'menu_id' => 1,
                    'title' => 'Paramètres',
                    'url'=> '/dashboard/settings',
                    'icon_class'=> 'glyphicon-cog',
                    'parent_id'=> NULL,
                    'order'=> 5
                ],
                [
                    'menu_id' => 1,
                    'title' => 'Posts',
                    'url'=> '/dashboard/posts',
                    'icon_class'=> 'glyphicon-pencil',
                    'parent_id'=> NULL,
                    'order'=> 6,
                ],
                [
                    'menu_id' => 1,
                    'title' => 'Pages',
                    'url'=> '/dashboard/pages',
                    'icon_class'=> 'glyphicon-file',
                    'parent_id'=> NULL,
                    'order'=> 7
                ]

            ],
            'menu-principal-header' => [
                [
                    'menu_id' => 2,
                    'title' => 'Ligues Compétitives',
                    'url' => '/ligues-competitives',
                    'icon_class'=> NULL,
                    'parent_id'=> NULL,
                    'order'=> 15
                ],
                [
                    'menu_id' => 2,
                    'title' => 'Ligues Amicales',
                    'url' => '/ligues-amicales',
                    'icon_class'=> NULL,
                    'parent_id'=> NULL,
                    'order'=> 16
                ],
                [
                    'menu_id' => 2,
                    'title' => 'Statistiques',
                    'url' => '/statistiques',
                    'icon_class'=> NULL,
                    'parent_id'=> NULL,
                    'order'=>17
                ],
                [
                    'menu_id' => 2,
                    'title' => 'Inscription',
                    'url' => '/inscription',
                    'icon_class'=> NULL,
                    'parent_id'=> NULL,
                    'order'=>18
                ],
                [
                    'menu_id' => 2,
                    'title' => 'Trouver un match',
                    'url' => '/trouver-un-match',
                    'icon_class'=> NULL,
                    'parent_id'=> NULL,
                    'order'=>19
                ],
                [
                    'menu_id' => 2,
                    'title' => 'Partenaires',
                    'url' => '/partenaires',
                    'icon_class'=> NULL,
                    'parent_id'=> NULL,
                    'order'=>20
                ],
            ],
            'menu-footer' => [
                [
                    'menu_id' => 3,
                    'title' => 'À Propos',
                    'url' => '/a-propos',
                    'icon_class'=> NULL,
                    'parent_id'=> NULL,
                    'order'=>21
                ],
                [
                    'menu_id' => 3,
                    'title' => 'Emplois',
                    'url' => '/emplois',
                    'icon_class'=> NULL,
                    'parent_id'=> NULL,
                    'order'=>22
                ],
                [
                    'menu_id' => 3,
                    'title' => 'Contactez-nous',
                    'url' => '/contactez-nous',
                    'icon_class'=> NULL,
                    'parent_id'=> NULL,
                    'order'=>23
                ],
                [
                    'menu_id' => 3,
                    'title' => 'Faire un paiement',
                    'url' => '/faire-un-paiement',
                    'icon_class'=> NULL,
                    'parent_id'=> NULL,
                    'order'=>24
                ],
                [
                    'menu_id' => 3,
                    'title' => 'Règlements',
                    'url' => '/reglements',
                    'icon_class'=> NULL,
                    'parent_id'=> NULL,
                    'order'=>25
                ],
            ]
        ];



        foreach ($menus_db as $m) {
            foreach ($menus[$m->name] as $mi) {
                MenuItem::create($mi);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
