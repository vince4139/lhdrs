<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

use App\Season;

class FillArenaSeasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $seasons = Season::all();

        foreach ($seasons as $season) {
            if($season->arena_id !== null)
            {
                DB::table('arenas_seasons')->insert([
                    "arena_id" => $season->arena_id,
                    "season_id" => $season->id
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
