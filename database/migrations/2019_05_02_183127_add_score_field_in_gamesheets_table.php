<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScoreFieldInGamesheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gamesheets', function (Blueprint $table) {
            $table->tinyInteger('team_home_score')->unsigned()->default(0)->after('team_home_shotout');
            $table->tinyInteger('team_away_score')->unsigned()->default(0)->after('team_away_shotout');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gamesheets', function (Blueprint $table) {
            $table->dropColumn(['team_home_score', 'team_away_score']);
        });
    }
}
