<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\PlayerStat;

class UpdatePlayersStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('players_stats', function($table) {
            $table->integer('player_id')->unsigned()->nullable($value=true)->change();
            $table->integer('league_id')->unsigned()->nullable($value=true)->change();
            $table->integer('season_id')->unsigned()->nullable($value=true)->change();
        });

        $stats_league_update = [355,359,362,366,369,397,401,405,409,411,414,417,423,427,434,441,452,457,461,467,471,476,482,487,490,495,506,512,520,529,531,538,540,558,559,563,564,565,577,579,594,630,631,632,633,636,650,652,653,662,663,665,668,674,356,360,363,367,370,373,398,402,406,410,412,415,418,424,428,431,436,438,442,445,448,458,462,465,468,472,478,483,488,491,496,507,521,524,530,533,541,548,552,553,560,585,590,595,596,601,613,651,654,657,666,669,358,365,371,372,400,404,408,420,422,426,429,430,433,435,440,444,447,450,451,454,456,460,464,470,474,475,480,481,485,486,493,494,498,500,502,503,505,508,510,511,513,514,515,516,517,518,519,523,526,527,528,532,535,536,537,539,542,543,544,545,546,547,550,551,555,556,557,562,566,567,568,569,570,571,573,574,575,576,578,581,582,584,586,587,588,589,592,593,597,598,599,600,602,603,605,606,607,608,609,610,611,612,615,616,617,619,620,621,622,623,624,625,626,628,629,634,635,637,638,639,640,641,642,643,644,645,646,649,656,659,661,664,667,670,672,676,2556,2557,2558,357,361,364,368,399,403,407,413,416,419,421,425,432,437,439,443,446,449,453,455,459,463,466,469,473,477,479,484,489,492,497,499,501,504,509,522,525,534,549,554,561,572,580,591,614,618,627,647,655,658,660,671,673,675, 2556,2557,2558];
        foreach ($stats_league_update as $slu) {
            $stat = App\PlayerStat::find($slu);

            $stat->league_id = NULL;

            $stat->save();
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('players_stats', function($table) {
            $table->integer('player_id')->unsigned()->change();
            $table->integer('league_id')->unsigned()->change();
            $table->integer('season_id')->unsigned()->change();
        });
    }
}
