<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameGoalAssistOneAndAssistTwoColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goals', function($table) {
            $table->renameColumn('goal', 'goal_player_id');
            $table->renameColumn('assist_one', 'assist_one_player_id');
            $table->renameColumn('assist_two', 'assist_two_player_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goals', function($table) {
            $table->renameColumn('goal_player_id', 'goal');
            $table->renameColumn('assist_one_player_id', 'assist_one');
            $table->renameColumn('assist_two_player_id', 'assist_two');
        });
    }
}
