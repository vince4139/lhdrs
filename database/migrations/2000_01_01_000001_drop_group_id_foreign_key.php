<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropGroupIdForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_groups', function($table) {
            $table->dropForeign('fk_users_groups_groups1');
            $table->dropForeign('fk_users_groups_users1');
        });

        Schema::rename('users_groups', 'user_roles');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::rename('user_roles', 'user_groups');

        Schema::table('users_groups', function($table) {
            $table->foreign('role_id', 'fk_users_groups_groups1')
                  ->references('id')->on('roles')
                  ->onDelete('cascade');

            $table->foreign('user_id', 'fk_users_groups_users1')
                  ->references('id')->on('users')->onDelete('cascade');
        });
    }
}
