<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Player;

class ColumnsTypesUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $players = Player::all();

        foreach ($players as $p) {
            if($p->birthdate == '0000-00-00') {
                $p->birthdate = NULL;
            }
            
            $p->save();
        }

        Schema::table('game_reports', function($table) {
            $table->unsignedInteger('id')->change();
            $table->bigInteger('game_id')->unsigned()->change();
        });

        Schema::table('gamesheets', function($table) {
            $table->dropPrimary('game_id');
            $table->bigInteger('game_id')->unsigned()->change();

        
            $table->unsignedInteger('user_id')->change();
            
            $table->unsignedInteger('star_three')->change();
            $table->unsignedInteger('star_two')->change();
            $table->unsignedInteger('star_one')->change();

            
            
        });

        Schema::table('goals', function($table) {
            $table->unsignedBigInteger('id')->change();
            $table->bigInteger('game_id', 20)->unsigned()->change();
            $table->unsignedInteger('goal')->change();
            $table->unsignedInteger('assist_one')->change();
            $table->unsignedInteger('assist_two')->change();
        });

        Schema::table('ins_competitif', function($table) {
            $table->unsignedInteger('id')->change();
            $table->unsignedInteger('league_id')->change();
            $table->unsignedInteger('player_id')->change();
        });

        Schema::table('leagues', function($table) {
            $table->increments('id')->change();
            $table->unsignedInteger('type')->change();
            $table->unsignedInteger('leagues_order')->change();
            $table->timestamps();
        });

        Schema::table('leagues_order', function($table) {
            $table->unsignedInteger('id')->change();
        });

        Schema::table('news_bar', function($table) {
            $table->unsignedInteger('id')->change();
        });

        Schema::table('pages', function($table) {
            $table->unsignedInteger('id')->change();
            $table->unsignedInteger('league_id')->change();
            $table->unsignedInteger('child_of')->change();
            $table->timestamps();
        });

        Schema::table('penaltys', function($table) {
            $table->unsignedBigInteger('id')->change();
            $table->bigInteger('game_id', 20)->unsigned()->change();
            $table->unsignedInteger('player')->change();
        });

        Schema::table('posts', function($table) {
            $table->unsignedInteger('entry_id')->change();
            $table->unsignedInteger('league_id')->change();
        });

        Schema::table('players', function($table) {
            $table->increments('id')->change();
        });

        Schema::table('publicity', function($table) {
            $table->unsignedInteger('id')->change();
        });

        Schema::table('publicity_leagues', function($table) {
            $table->unsignedInteger('id')->change();
            $table->unsignedInteger('league_id')->change();
        });

        Schema::table('schedule', function($table) {
            $table->bigIncrements('id')->unsigned()->change();
            $table->unsignedInteger('publicity_home')->change();
            $table->unsignedInteger('publicity_away')->change();
            $table->unsignedInteger('league_id')->change();
            $table->unsignedInteger('season_id')->change();
        });

        Schema::table('seasons', function($table) {
            $table->increments('id')->change();
            $table->unsignedInteger('league_id')->change();
        });

        Schema::table('standings', function($table) {
            $table->unsignedInteger('id')->change();
            $table->unsignedInteger('league_id')->change();
            $table->unsignedInteger('season_id')->change();
            $table->unsignedInteger('team_id')->change();
        });

        Schema::table('substitution', function($table) {
            $table->unsignedInteger('id')->change();
            $table->unsignedInteger('player_id')->change();
        });

        Schema::table('teams', function($table) {
            $table->unsignedInteger('id')->change();
            $table->unsignedInteger('league_id')->change();
        });

        Schema::table('transactions', function($table) {
            $table->unsignedInteger('id')->change();
            $table->unsignedInteger('player_id')->change();
            $table->unsignedInteger('marqueur_id')->change();
        });

        Schema::table('users', function($table) {
            // $table->unsignedInteger('id')->change();
            $table->unsignedInteger('player_id')->change();
            $table->timestamps();
        });

        Schema::table('users_infos_temp', function($table) {
            $table->unsignedInteger('id')->change();
            $table->unsignedInteger('user_id')->change();
            $table->unsignedInteger('player_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_reports', function($table) {
            $table->biginteger('id')->change();
            $table->biginteger('game_id')->change();
        });

        Schema::table('gamesheets', function($table) {
            $table->integer('user_id')->change();
            $table->integer('game_id')->change();
            $table->bigInteger('star_three')->change();
            $table->bigInteger('star_two')->change();
            $table->bigInteger('star_one')->change();
        });

        Schema::table('goals', function($table) {
            $table->bigInteger('id')->change();
            $table->bigInteger('game_id')->change();
            $table->bigInteger('goal')->change();
            $table->bigInteger('assist_one')->change();
            $table->bigInteger('assist_two')->change();
        });

        Schema::table('ins_competitif', function($table) {
            $table->bigInteger('id')->change();
            $table->bigInteger('league_id')->change();
            $table->bigInteger('player_id')->change();
        });

        Schema::table('leagues', function($table) {
            $table->integer('id')->change();
            $table->integer('type')->change();
            $table->bigInteger('leagues_order')->change();
        });

        Schema::table('leagues_order', function($table) {
            $table->bigInteger('id')->change();
        });

        Schema::table('news_bar', function($table) {
            $table->bigInteger('id')->change();
        });

        Schema::table('pages', function($table) {
            $table->bigInteger('id')->change();
            $table->bigInteger('league_id')->change();
            $table->bigInteger('child_of')->change();
        });

        Schema::table('penaltys', function($table) {
            $table->integer('id')->change();
            $table->integer('game_id')->change();
            $table->bigInteger('player')->change();
        });

        Schema::table('posts', function($table) {
            $table->bigInteger('entry_id')->change();
            $table->bigInteger('league_id')->change();
        });

        Schema::table('publicity', function($table) {
            $table->bigInteger('id')->change();
        });

        Schema::table('publicity_leagues', function($table) {
            $table->bigInteger('id')->change();
            $table->bigInteger('league_id')->change();
        });

        Schema::table('schedule', function($table) {
            $table->integer('id')->change();
            $table->integer('publicity_home')->change();
            $table->integer('publicity_away')->change();
            $table->integer('league_id')->change();
            $table->integer('season_id')->change();
        });

        Schema::table('seasons', function($table) {
            $table->integer('id')->change();
            $table->integer('league_id')->change();
        });

        Schema::table('standings', function($table) {
            $table->integer('id')->change();
            $table->integer('league_id')->change();
            $table->integer('season_id')->change();
            $table->integer('team_id')->change();
        });

        Schema::table('substitution', function($table) {
            $table->bigInteger('id')->change();
            $table->bigInteger('player_id')->change();
            $table->integer('id')->change();
        });

        Schema::table('teams', function($table) {
            $table->integer('id')->change();
            $table->integer('league_id')->change();
        });

        Schema::table('transactions', function($table) {
            $table->bigInteger('id')->change();
            $table->bigInteger('player_id')->change();
            $table->bigInteger('marqueur_id')->change();
        });

        Schema::table('users', function($table) {
            $table->integer('id')->change();
            $table->integer('role_id')->change();
            $table->bigInteger('player_id')->change();
        });

        Schema::table('users_infos_temp', function($table) {
            $table->bigInteger('id')->change();
            $table->bigInteger('user_id')->change();
            $table->bigInteger('player_id')->change();
        });
    }
}
