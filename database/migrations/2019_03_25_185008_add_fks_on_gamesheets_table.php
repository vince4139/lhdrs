<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFksOnGamesheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gamesheets', function($table) {
            $table->foreign('user_id', 'fk_gamesheets_user_id')
                  ->references('id')->on('users')
                  ->onDelete('set null');

            $table->foreign('star_three', 'fk_star_three_player_id')
                  ->references('id')->on('players')
                  ->onDelete('set null');

            $table->foreign('star_two', 'fk_star_two_player_id')
                  ->references('id')->on('players')
                  ->onDelete('set null');

            $table->foreign('star_one', 'fk_star_one_player_id')
                  ->references('id')->on('players')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gamesheets', function($table) {
            $table->dropForeign('fk_gamesheets_user_id');
            $table->dropForeign('fk_star_three_player_id');
            $table->dropForeign('fk_star_two_player_id');
            $table->dropForeign('fk_star_one_player_id');
        });
    }
}
