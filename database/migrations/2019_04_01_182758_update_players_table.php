<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Player;

class UpdatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $players = Player::all();

        foreach ($players as $p) {
            if($p->favorite_position == 0) {
            	$p->favorite_position = NULL;
            	$p->save();
            }
        }


        Schema::table('players', function($table) {
            $table->integer('favorite_position')->unsigned()->change();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('players', function($table) {
            $table->integer('favorite_position')->change();
        });
    }
}
