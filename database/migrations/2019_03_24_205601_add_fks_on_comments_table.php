<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFksOnCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function($table) {

            $table->integer('page_id')->nullable($value=true)->unsigned()->change();
            $table->integer('post_id')->nullable($value=true)->unsigned()->change();

            $table->foreign('page_id', 'fk_page_id')
                  ->references('id')->on('pages')
                  ->onDelete('set null');

            $table->foreign('post_id', 'fk_post_id')
                  ->references('entry_id')->on('posts')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function($table) {
            $table->integer('page_id')->nullable($value=true)->change();
            $table->integer('post_id')->nullable($value=true)->change();

            $table->dropForeign('fk_page_id');
            $table->dropForeign('fk_post_id');
        });
    }
}
