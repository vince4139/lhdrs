<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use App\League;
use App\Season;

class NormalizePlayersStatsContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        $infos_to_replace = [
            [ 
                'name' => 'Été 2012',
                'day' => 'Jeudi',
                'type' => 1,
                'game_start' => '22:00',
                'game_end' => '23:30',
                'arena' => 'Complexe sportif Longueil',
                'notes' => '',
                'level' => '',
                'pub_home' => 0,
                'pub_away' => 0,
                'play_s_price' => 0.00,
                'goal_s_price' => 0.00,
                'play_g_price' => 0.00,
                'goal_g_price' => 0.00,
                'games' => 0,
                'players' => '',
                'goalie' => '',
                'season_id' => 0,
                'i_players' => 0,
                'i_goalies' => 0,
                'find_match' => 0,
                'leagues_order' => 0,
                'status' => 2,
                'begin' => '2012-05-03'
            ],
            [
                'name' => 'Automne 2012',
                'day' => 'Jeudi',
                'type' => 1,
                'game_start' => '22:00',
                'game_end' => '23:30',
                'arena' => 'Centre sportif Rosanne-Laflamme',
                'notes' => '',
                'level' => '',
                'pub_home' => 0,
                'pub_away' => 0,
                'play_s_price' => 0.00,
                'goal_s_price' => 0.00,
                'play_g_price' => 0.00,
                'goal_g_price' => 0.00,
                'games' => 0,
                'players' => '',
                'goalie' => '',
                'season_id' => 0,
                'i_players' => 0,
                'i_goalies' => 0,
                'find_match' => 0,
                'leagues_order' => 0,
                'status' => 2,
                'begin' => '2012-09-03'
            ],
            [ 
                'name' => 'Été 2013',
                'day' => 'Jeudi',
                'type' => 1,
                'game_start' => '22:00',
                'game_end' => '23:30',
                'arena' => 'Complexe sportif Longueil',
                'notes' => '',
                'level' => '',
                'pub_home' => 0,
                'pub_away' => 0,
                'play_s_price' => 0.00,
                'goal_s_price' => 0.00,
                'play_g_price' => 0.00,
                'goal_g_price' => 0.00,
                'games' => 0,
                'players' => '',
                'goalie' => '',
                'season_id' => 0,
                'i_players' => 0,
                'i_goalies' => 0,
                'find_match' => 0,
                'leagues_order' => 0,
                'status' => 2,
                'begin' => '2013-05-03'
            ], 
            [
                'name' => 'Hiver 2013',
                'day' => 'Jeudi',
                'type' => 1,
                'game_start' => '22:15',
                'game_end' => '23:45',
                'arena' => 'Les 4 Glaces',
                'notes' => '',
                'level' => '',
                'pub_home' => 0,
                'pub_away' => 0,
                'play_s_price' => 0.00,
                'goal_s_price' => 0.00,
                'play_g_price' => 0.00,
                'goal_g_price' => 0.00,
                'games' => 0,
                'players' => '',
                'goalie' => '',
                'season_id' => 0,
                'i_players' => 0,
                'i_goalies' => 0,
                'find_match' => 0,
                'leagues_order' => 0,
                'status' => 2,
                'begin' => '2013-09-03'
            ]
        ];

        foreach($infos_to_replace as $infos) {
            $season = new Season;
            $league = new League;

            $season->name = $infos['name'];
            $season->begin = $infos['begin'];
            $season->league_id = 0;
            $season->save();

            $league->type = $infos['type'];
            $league->name = $infos['name'];
            $league->day = $infos['day'];
            $league->game_start = $infos['game_start'];
            $league->game_end = $infos['game_end'];
            $league->game_hour = '';
            $league->arena = $infos['arena'];
            $league->notes = $infos['notes'];
            $league->level = $infos['level'];
            $league->publicity_home = $infos['pub_home'];
            $league->publicity_away = $infos['pub_away'];
            $league->player_season_price = $infos['play_s_price'];
            $league->goalie_season_price = $infos['goal_s_price'];
            $league->player_game_price = $infos['play_g_price'];
            $league->goalie_game_price = $infos['goal_g_price'];
            $league->games_number = $infos['games'];
            $league->players = $infos['players'];
            $league->goalie = $infos['goalie'];
            $league->season_id = $season->id;
            $league->inscription_players = $infos['i_players'];
            $league->inscription_goalies = $infos['i_goalies'];
            $league->find_match = $infos['find_match'];
            $league->leagues_order = $infos['leagues_order'];
            $league->status = $infos['status'];
            $league->save();

            App\Season::where('id', $season->id)->update(['league_id' => $league->id]);

            $affected = DB::update('update players_stats set season_id=' . $season->id . ' where season_id = ?', [$infos['name']]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
