<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkGoalTypeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goals', function($table) {
            $table->integer('goal_type_id')->nullable($value=true)->unsigned()->change();
            $table->integer('goal')->nullable($value=true)->unsigned()->change();
            $table->integer('assist_one')->nullable($value=true)->unsigned()->change();
            $table->integer('assist_two')->nullable($value=true)->unsigned()->change();

            $table->foreign('goal_type_id', 'fk_goal_type_id')
                  ->references('id')->on('goal_types')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goals', function($table) {

            $table->integer('goal_type_id')->unsigned()->change();
            $table->integer('goal')->unsigned()->change();
            $table->integer('assist_one')->unsigned()->change();
            $table->integer('assist_two')->unsigned()->change();

            $table->dropForeign('fk_goal_type_id');
        });
    }
}
