<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

use App\Season;
use App\Player;

class FillInscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $seasons = Season::all();

        foreach ($seasons as $season) {
            $players = explode(',', $season->players);
            $goalies = explode(',', $season->goalies);

            foreach ($players as $player_id) {

                // check if player exists
                $player = Player::find($player_id);
                if($player != null) {
                    DB::table('inscriptions_test')->insert([
                        "player_id" => $player_id,
                        "season_id" => $season->id,
                        "is_goalie" => 0
                    ]);
                }
            }

            foreach ($goalies as $goalie_id) {
                // check if player exists
                $goalie = Player::find($goalie_id);
                
                if($goalie != null) {
                    DB::table('inscriptions_test')->insert([
                        "player_id" => $goalie_id,
                        "season_id" => $season->id,
                        "is_goalie" => 1
                    ]);
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
