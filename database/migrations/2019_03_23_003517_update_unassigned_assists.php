<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Goal;

class UpdateUnassignedAssists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** These goals are linked to a unexistant player. but got an assist on them */
        $goals_to_update = [1950,3352];

        foreach ($goals_to_update as $gtu) {
            $goal = Goal::find($gtu);

            $goal->assist_one_player_id = NULL;

            $goal->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
