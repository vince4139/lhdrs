<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsAndTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('posts') && Schema::hasTable('news')) {
            Schema::rename('news', 'posts');
        }

        Schema::table('users', function($table) {
            $table->renameColumn('remember_code', 'remember_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        if(Schema::hasTable('posts') && !Schema::hasTable('news')) {
            Schema::rename('posts', 'news');
        }


        Schema::table('users', function($table) {
            $table->renameColumn('remember_token', 'remember_code');
        });
    }
}
