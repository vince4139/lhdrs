<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FormatTablesTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('absences', function($table) {
            $table->biginteger('id')->unsigned()->change();
            $table->integer('player_id')->unsigned()->change();
            $table->biginteger('game_id')->unsigned()->change();
            $table->integer('sub_id')->unsigned()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('absences', function($table) {
            $table->integer('id')->change();
            $table->biginteger('player_id')->change();
            $table->biginteger('game_id')->change();
            $table->biginteger('sub_id')->change();
        });
    }
}
