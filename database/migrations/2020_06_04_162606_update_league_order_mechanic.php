<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\League;

class UpdateLeagueOrderMechanic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leagues', function (Blueprint $table) {
            $table->dropForeign('fk_leagues_league_order_id');
        });

        Schema::drop('leagues_order');

        $leagues = League::all();

        foreach ($leagues as $league) {
            if($league->status == 1) { // ligue active
                $org_number = 3000;
            } else if($league->status == 2) { // ligue inactive
                $org_number = 1000;
            } else { // ligue no débutée
                $org_number = 2000;
            }

            $league->leagues_order = $league->leagues_order + $org_number;
            $league->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
