<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmergencyContactInfosToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('users', 'emergency_contact_name')) {
            Schema::table('users', function($table) {
                $table->string('emergency_contact_name', 50)->after('zipcode');
            });
        }

        if(!Schema::hasColumn('users', 'emergency_contact_phone')) {
            Schema::table('users', function($table) {
                $table->string('emergency_contact_phone', 50)->after('emergency_contact_name');
            });
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('users', 'emergency_contact_name')) {
            Schema::table('users', function($table) {
                $table->dropColumn('emergency_contact_name');
            });
        }

        if(Schema::hasColumn('users', 'emergency_contact_phone')) {
            Schema::table('users', function($table) {
                $table->dropColumn('emergency_contact_phone');
            });
        }       
    }
}
