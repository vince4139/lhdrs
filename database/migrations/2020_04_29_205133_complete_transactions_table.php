<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\League;
use App\Player;
use App\Transaction;
use App\Team;



class CompleteTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $leagues = League::all();
        $teams = Team::all();

        foreach ($leagues as $l) {
            // check in the leagues
            if(!is_null($l->players)) {
                $players = explode(',', $l->players);

                foreach ($players as $p_id) {
                    $player = Player::where('id', '=', $p_id)->first();
                    if(!is_null($player)) {
                        $transaction = new Transaction;

                        $transaction->type = 1;
                        $transaction->player_id = $p_id;
                        $transaction->amount = $l->player_season_price;
                        $transaction->note = 'Inscription à ' . $l->name;
                        $transaction->save();
                    }
                }
            }

            if(!is_null($l->goalie)) {
                $goalies = explode(',', $l->goalie);

                foreach ($goalies as $g_id) {
                    $goalie = Player::where('id', '=', $g_id)->first();
                    if(!is_null($goalie)) {
                        $transaction = new Transaction;

                        $transaction->type = 1;
                        $transaction->player_id = $g_id;
                        $transaction->amount = $l->goalie_season_price;
                        $transaction->note = 'Inscription à ' . $l->name;
                        $transaction->save();
                    }
                }
            }

        }

        foreach ($teams as $t) {
            //check in teams
            $league = League::find($t->league_id);

            if(!is_null($l->players)) {
                $players = explode(',', $t->players);

                foreach ($players as $p_id) {
                    $player = Player::where('id', '=', $p_id)->first();
                    if(!is_null($player)) {
                        $transaction = new Transaction;

                        $transaction->type = 1;
                        $transaction->player_id = $p_id;
                        $transaction->amount = $league->player_season_price;
                        $transaction->note = "Inscription dans l'équipe " . $t->name . ' lors de la saison ' . $league->name;
                        $transaction->save();
                    }
                }
            }

            if(!is_null($l->goalie)) {
                $goalies = explode(',', $t->goalie);

                foreach ($goalies as $g_id) {
                    $goalie = Player::where('id', '=', $g_id)->first();
                    if(!is_null($goalie)) {
                        $transaction = new Transaction;

                        $transaction->type = 1;
                        $transaction->player_id = $g_id;
                        $transaction->amount = $league->goalie_season_price;
                        $transaction->note = "Inscription dans l'équipe " . $t->name . ' lors de la saison ' . $league->name;
                        $transaction->save();
                    }
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
