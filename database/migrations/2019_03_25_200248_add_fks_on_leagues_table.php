<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFksOnLeaguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('leagues', function($table){
            $table->integer('publicity_home')->unsigned()->nullable($value=true)->change();
            $table->integer('publicity_away')->unsigned()->nullable($value=true)->change();
            $table->integer('season_id')->unsigned()->nullable($value=true)->change();
            $table->integer('leagues_order')->unsigned()->nullable($value=true)->change();
            $table->integer('status')->unsigned()->nullable($value=true)->change();
        });

        Schema::table('leagues', function($table) {
            $table->foreign('publicity_home', 'fk_leagues_publicity_home_id')
                  ->references('id')->on('publicity')
                  ->onDelete('set null');

            $table->foreign('publicity_away', 'fk_leagues_publicity_away_id')
                  ->references('id')->on('publicity')
                  ->onDelete('set null');

            $table->foreign('season_id', 'fk_leagues_season_id')
                  ->references('id')->on('seasons')
                  ->onDelete('set null');

            $table->foreign('leagues_order', 'fk_leagues_league_order_id')
                  ->references('id')->on('leagues_order')
                  ->onDelete('set null');

            $table->foreign('status', 'fk_leagues_status_id')
                  ->references('id')->on('league_statuses')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leagues', function($table) {
            $table->dropForeign('fk_leagues_publicity_home_id');
            $table->dropForeign('fk_leagues_publicity_away_id');
            $table->dropForeign('fk_leagues_season_id');
            $table->dropForeign('fk_leagues_league_order_id');
            $table->dropForeign('fk_leagues_status_id');
        });
    }
}
