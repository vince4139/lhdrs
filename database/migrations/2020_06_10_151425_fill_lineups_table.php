<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

use App\Game;
use App\Player;

class FillLineupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $games = Game::all();

        foreach ($games as $game) {
            $players_categories = [
                "away_players" => explode(',' ,$game->away_players),
                "away_players_subs" => explode(',' ,$game->away_players_subs),
                "away_goalie" => $game->away_goalie,
                "away_goalie_subs" =>$game->away_goalie_subs,
                "home_players" => explode(',' ,$game->home_players),
                "home_players_subs" => explode(',' ,$game->home_players_subs),
                "home_goalie" => $game->home_goalie,
                "home_goalie_subs" => $game->home_goalie_subs,
            ];

            foreach ($players_categories as $category => $ids) {

                if(strpos($category, 'goalie') !== false) 
                {
                    // check if player exists
                    $goalie = Player::find($ids);
                    if($goalie != null) {
                        DB::table('lineups_test')->insert([
                            "player_id" => $ids,
                            "schedule_id" => $game->id,
                            "is_goalie" => 1,
                            "is_substitute" => (strpos($category, 'subs') !== false) ? 1 : 0,
                            "is_home" => (strpos($category, 'home') !== false) ? 1 : 0
                        ]);
                    }
                }
                else 
                {
                    foreach ($ids as $id) {
                        // check if player exists
                        $player = Player::find($id);
                        if($player != null) {
                            DB::table('lineups_test')->insert([
                                "player_id" => $id,
                                "schedule_id" => $game->id,
                                "is_goalie" => 0,
                                "is_substitute" => (strpos($category, 'subs') !== false) ? 1 : 0,
                                "is_home" => (strpos($category, 'home') !== false) ? 1 : 0
                            ]);
                        }
                    }
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
