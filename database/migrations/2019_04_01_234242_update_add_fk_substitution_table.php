<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use App\Substitution;
use App\Player;

class UpdateAddFkSubstitutionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $subs = App\Substitution::all();

        Schema::table('substitution', function($table) {
            $table->integer('player_id')->nullable($value=true)->unsigned()->change();
        });

        foreach ($subs as $s) {
            $player = App\Player::where('id', '=', $s->player_id)->first();
            if($player === null) {
                $s->player_id = null;
                $s->save();
            }
        }

        Schema::table('substitution', function($table) {
            $table->foreign('player_id', 'fk_substitution_player_id')
                  ->references('id')->on('players')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('substitution', function($table) {
            $table->integer('player_id')->unsigned()->change();

            $table->dropForeign('fk_substitution_player_id');
        });
    }
}
