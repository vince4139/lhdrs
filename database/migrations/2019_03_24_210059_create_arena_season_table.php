<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArenaSeasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('arenas_seasons')) {
            Schema::create('arenas_seasons', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('arena_id')->unsigned();
                $table->integer('season_id')->unsigned();
                $table->timestamps();

                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->change();
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->change();

                $table->foreign('arena_id')->references('id')->on('arenas')->onDelete('cascade');
                $table->foreign('season_id')->references('id')->on('seasons')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('arenas_seasons')) {
            Schema::table('arenas_seasons', function($table) {
                $table->dropForeign(['arena_id', 'league_id']);
            });
            Schema::dropIfExists('arenas_seasons');
        }
    }
}
