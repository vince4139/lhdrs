<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Game;

class UpdateGamesheetsWithScores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $games = Game::has('gamesheet')->get();

        foreach($games as $g) {
            $home_score = 0;
            $away_score = 0;


            $goals = $g->goals;

            foreach($goals as $go) {

                if($go->team_home) {
                    $home_score++;
                } else {
                    $away_score++;
                }
            }


            $gamesheet = $g->gamesheet;

            $gamesheet->team_home_score = $home_score;
            $gamesheet->team_away_score = $away_score;

            $g->gamesheet()->save($gamesheet);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
