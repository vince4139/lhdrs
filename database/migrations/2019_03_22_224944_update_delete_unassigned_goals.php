<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Goal;

class UpdateDeleteUnassignedGoals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** This goal is linked to a unexistant player */
        $goal_to_delete = App\Goal::destroy(1951);

        /** These goals are linked to a unexistant player. but got an assist on them */
        $goals_to_update = [2039,4224];

        foreach ($goals_to_update as $gtu) {
            $goal = Goal::find($gtu);

            $goal->goal_player_id = NULL;

            $goal->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
