<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Arena;

class UpdateArenasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $records_to_delete = App\Arena::destroy([8,9,10,11,12,13,14,97,107,108,112,114,115,116]);

        $records_to_create = [
            [
                'name' => 'Isatis Sport Chambly',
                'address' => 3000,
                'street' => 'Boulevard Fréchette',
                'postal_code' => 'J3L 6Z6',
                'phone_number' => '450 447-3350',
                'url' => 'https://isatissport.com/',
                'location' => 'Chambly',

            ],
            [
                'name' => 'Complexe Sportif Sportscene',
                'address' => 539,
                'street' => 'Boulevard Sir-Wilfrid-Laurier,',
                'postal_code' => 'J3H 4X7',
                'phone_number' => '450 467-1731',
                'url' => 'http://csmsh.ca/sports-sur-glace/hockey/',
                'location' => 'Mont-Saint-Hilaire',
            ],
            [
                'name' => 'Complexe récréatif Gadbois',
                'address' => 5485,
                'street' => 'Chemin de la Côte-Saint-Paul',
                'postal_code' => 'H4C 1X3',
                'phone_number' => '514 872-2786',
                'url' => 'http://ville.montreal.qc.ca/portal/page?_pageid=7757,84739586&_dad=portal&_schema=PORTAL',
                'location' => 'Montréal',
            ],
            [
                'name' => 'Centre sportif Rosanne-Laflamme',
                'address' => 7405,
                'street' => 'Terrasse du Centre',
                'postal_code' => 'J3Y 7Z6',
                'phone_number' => '450 463-7100',
                'url' => 'https://www.longueuil.quebec/fr/centre-sportif-rosanne-laflamme',
                'location' => 'Saint-Hubert',
            ],
        ];

        $records_to_update = [
            [
                'id' => 98,
                'value' => 'Centre Sportif Duval Auto'
            ],
            [
                'id' => 100,
                'value' => 'Les 4 Glaces'
            ],
            [
                'id' => 102,
                'value' => 'Complexe sportif Longueuil'
            ]
        ];

        Schema::table('arenas', function($table) {
            $table->integer('id')->unsigned()->autoIncrement()->change();
        });

        foreach ($records_to_create as $rtc) {
            $arena = new App\Arena;

            $arena->name = $rtc['name'];
            $arena->address = $rtc['address'];
            $arena->street = $rtc['street'];
            $arena->postal_code = $rtc['postal_code'];
            $arena->phone_number = $rtc['phone_number'];
            $arena->url = $rtc['url'];
            $arena->location = $rtc['location'];

            $arena->save();
        }

        foreach ($records_to_update as $rtu) {
            $arena = App\Arena::find($rtu['id']);

            $arena->name = $rtu['value'];

            $arena->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
