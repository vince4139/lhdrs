<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOldForeigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gamesheets', function($table) {
            $table->dropForeign('gamesheets_ibfk_1');
        });

        Schema::table('teams', function($table) {
            $table->dropForeign('teams_ibfk_1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gamesheets', function($table) {
            $table->foreign('game_id', 'gamesheets_ibfk_1')
                  ->references('id')->on('schedule');
        });

        Schema::table('teams', function($table) {
            $table->foreign('league_id', 'teams_ibfk_1')
                  ->references('id')->on('leagues');
        });
    }
}
