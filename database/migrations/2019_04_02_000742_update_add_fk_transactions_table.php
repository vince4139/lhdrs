<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

use App\Transaction;

class UpdateAddFkTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::table('transactions', function($table) {
            $table->integer('id')->autoIncrement()->change();
            $table->integer('player_id')->nullable($value=true)->unsigned()->change();
            $table->integer('marqueur_id')->nullable($value=true)->unsigned()->change();
            $table->integer('type')->nullable($value=true)->unsigned()->change();
        });

        $transactions = Transaction::all();

        foreach ($transactions as $t) {
            if($t->player == null) {
                $t->player_id = NULL;

                $t->save();
            }

            if($t->marqueur == null) {
                $t->marqueur_id = NULL;

                $t->save();
            }
        }


    

        Schema::table('transactions', function($table) {
            $table->foreign('player_id', 'fk_transactions_player_id')
                  ->references('id')->on('players')
                  ->onDelete('set null');

            $table->foreign('marqueur_id', 'fk_transactions_marqueur_id')
                  ->references('id')->on('users')
                  ->onDelete('set null');

            $table->foreign('type', 'fk_transactions_type_id')
                  ->references('id')->on('transaction_types')
                  ->onDelete('set null');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function($table) {
            $table->integer('player_id')->unsigned()->change();
            $table->integer('marqueur_id')->unsigned()->change();
            $table->integer('type')->unsigned()->change();

            $table->dropForeign('fk_transactions_player_id');

            $table->dropForeign('fk_transactions_marqueur_id');

            $table->dropForeign('fk_transactions_type_id');
        });
    }
}
