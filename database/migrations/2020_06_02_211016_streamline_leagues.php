<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StreamlineLeagues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leagues', function (Blueprint $table) {
            $table->dropForeign('fk_arena_id');
            $table->dropForeign('fk_leagues_season_id');
            $table->dropColumn([
                'arena', 
                'game_start', 
                'game_end', 
                'game_hour', 
                'notes', 
                'level', 
                'players', 
                'goalie', 
                'games_number', 
                'inscription_players', 
                'inscription_goalies',
                'find_match',
                'season_id',
                'player_season_price',
                'goalie_season_price',
                'player_game_price',
                'goalie_game_price',]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
