<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFksPlayersStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('players_stats', function($table) {
            $table->foreign('player_id', 'fk_players_stats_player_id')
                  ->references('id')->on('players')
                  ->onDelete('set null');

            $table->foreign('league_id', 'fk_players_stats_league_id')
                  ->references('id')->on('leagues')
                  ->onDelete('set null');

            $table->foreign('team_id', 'fk_players_stats_team_id')
                  ->references('id')->on('teams')
                  ->onDelete('set null');

            $table->foreign('season_id', 'fk_players_stats_season_id')
                  ->references('id')->on('seasons')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('players_stats', function($table) {
             $table->dropForeign('fk_players_stats_player_id');
             $table->dropForeign('fk_players_stats_league_id');
             $table->dropForeign('fk_players_stats_team_id');
             $table->dropForeign('fk_players_stats_season_id');
        });
    }
}
