<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Goal;

class DeleteWeirdGoals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $goals_to_remove = [488, 3176, 13010, 13068, 14844, 15907, 17888, 20221, 20461, 29833, 34238, 34239];
        $goals = App\Goal::find($goals_to_remove);

        if(!is_null($goals)) {
            App\Goal::destroy($goals_to_remove);
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
