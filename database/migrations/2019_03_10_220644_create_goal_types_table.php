<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\GoalType;

class CreateGoalTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $goal_types = [
            [
                'display_name' => 'But en égalité numérique',
                'short_name' => ''
            ],
            [
                'display_name' => 'But en avantage numérique',
                'short_name' => 'A.N.'
            ],
            [
                'display_name' => 'But en infériorité numérique',
                'short_name' => 'I.N.'
            ],
            [
                'display_name' => 'But en tir de pénalité',
                'short_name' => 'P.S.'
            ],
            [
                'display_name' => '???',
                'short_name' => ''
            ]
        ];

        Schema::create('goal_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('display_name');
            $table->string('short_name');
        });

        foreach ($goal_types as $gt) {
            $goal_type = new GoalType;

            $goal_type->display_name = $gt['display_name'];
            $goal_type->short_name = $gt['short_name'];

            $goal_type->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goal_types');
    }
}
