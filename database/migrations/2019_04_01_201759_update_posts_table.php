<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Post;

class UpdatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('posts', function($table) {
            $table->dropColumn('draft');
            $table->integer('league_id')->nullable($value=true)->unsigned()->change();
            

            $table->timestamps();

            $table->renameColumn('entry_id', 'id');
            $table->renameColumn('entry_title', 'title');
            $table->renameColumn('entry_body', 'body');
            $table->renameColumn('entry_body_more', 'excerpt');
            $table->string('slug');

            $table->enum('status', ['DRAFT', 'PUBLISHED', 'PENDING'])->nullable($value=true);
            $table->integer('author_id')->unsigned()->nullable(true);
            $table->string('meta_description')->nullable(true);
            $table->string('meta_keywords')->nullable(true);
            $table->string('seo_title')->nullable(true);
        });

        $posts = Post::all();

        foreach ($posts as $post) {
            $post->league_id = NULL;
            $post->status = NULL;
            $post->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function($table) {
            $table->integer('league_id')->unsigned()->change();
            $table->integer('status')->nullable($value=true)->change();

            $table->dropTimestamps();

            $table->renameColumn('id', 'entry_id');
            $table->renameColumn('title', 'entry_title');
            $table->renameColumn('body', 'entry_body');
            $table->renameColumn('excerpt', 'entry_body_more');
            $table->renameColumn('status', 'draft');
        });
    }
}
