<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\PlayerPosition;

class CreatePlayerPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_positions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('abbreviation');
        });

        $positions = [
            [
                'name' => 'Centre',
                'abbreviation' => 'C'
            ],
            [
                'name' => 'Ailier Gauche',
                'abbreviation' => 'AG'
            ],
            [
                'name' => 'Ailier Droit',
                'abbreviation' => 'AD'
            ],
            [
                'name' => 'Défenseur',
                'abbreviation' => 'D'
            ],
            [
                'name' => 'Gardien',
                'abbreviation' => 'G'
            ]
        ];

        foreach ($positions as $pos) {
            $position = new PlayerPosition;

            $position->name = $pos['name'];
            $position->abbreviation = $pos['abbreviation'];

            $position->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_positions');
    }
}
