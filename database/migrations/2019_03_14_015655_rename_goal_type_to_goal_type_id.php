<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameGoalTypeToGoalTypeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('goals', 'goal_type') && !Schema::hasColumn('goals', 'goal_type_id')) {
            Schema::table('goals', function($table) {
                $table->renameColumn('goal_type', 'goal_type_id')->unsigned()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('goals', 'goal_type_id') && !Schema::hasColumn('goals', 'goal_type')) {
            Schema::table('goals', function($table) {
                $table->integer('goal_type_id')->change();
                $table->renameColumn('goal_type_id', 'goal_type');
            });
        }
    }
}
