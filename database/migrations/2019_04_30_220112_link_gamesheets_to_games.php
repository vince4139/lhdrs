<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LinkGamesheetsToGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gamesheets', function (Blueprint $table) {
            $table->bigInteger('game_id')->unsigned()->nullable()->change();
        });

        Schema::table('gamesheets', function(Blueprint $table) {
            
            $table->foreign('game_id', 'fk_gamesheets_game_id')
                    ->references('id')
                    ->on('schedule')
                    ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gamesheets', function (Blueprint $table) {
            $table->dropForeign('fk_gamesheets_game_id');
            $table->primary('game_id');
        });
    }
}
