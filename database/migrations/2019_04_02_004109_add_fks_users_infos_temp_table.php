<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFksUsersInfosTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_infos_temp', function($table) {
            $table->foreign('user_id', 'fk_users_infos_temp_user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');

            $table->foreign('player_id', 'fk_users_infos_temp_player_id')
                  ->references('id')->on('players')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_infos_temp', function($table) {
            $table->dropForeign('fk_users_infos_temp_user_id');
            $table->dropForeign('fk_users_infos_temp_player_id');
        });
    }
}
