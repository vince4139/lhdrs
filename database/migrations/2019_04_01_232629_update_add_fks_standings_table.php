<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateAddFksStandingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      Schema::table('standings', function($table) {
          $table->integer('league_id')->nullable($value=true)->unsigned()->change();
          $table->integer('season_id')->nullable($value=true)->unsigned()->change();
          $table->integer('team_id')->nullable($value=true)->unsigned()->change();
      });


      $standings_to_fix = [31,32,33];

      foreach ($standings_to_fix as $stf) {
          DB::table('standings')->where('id', $stf)
                                  ->update(['league_id' => NULL, 'season_id' => NULL]);
      }

       
        Schema::table('standings', function($table) {
            $table->foreign('league_id', 'fk_standings_league_id')
                  ->references('id')->on('leagues')
                  ->onDelete('set null');

            $table->foreign('season_id', 'fk_standings_season_id')
                  ->references('id')->on('seasons')
                  ->onDelete('set null');

            $table->foreign('team_id', 'fk_standings_team_id')
                  ->references('id')->on('teams')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('standings', function($table) {
            $table->integer('league_id')->unsigned()->change();
            $table->integer('season_id')->unsigned()->change();
            $table->integer('team_id')->unsigned()->change();

            $table->dropForeign('fk_standings_league_id');

            $table->dropForeign('fk_standings_season_id');

            $table->dropForeign('fk_standings_team_id');
        });
    }
}
