<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/workbench-two-col', function () {
//     return view('pages.workbench_2col');
// });

// Route::get('/workbench-single-col', function () {
//     return view('pages.workbench_single');
// });

Auth::routes();

Route::get('/', 'PagesController@index')->name('index');
Route::get('{path}', 'PagesController@index')->where( 'path', '([A-z\d\-\/_.]+)?' );
Route::get('mailable', function() {
	$mail_infos = [
		'subject' => 'Activation du compte',
		'to' => 'vlabbe3941@gmail.com',
		'body' => "<p>Bonjour Testeur Test,</p> <p>Pour valider votre courriel, veuillez vous rendre sur la page suivante :</p>",
		'link' => "http://google.com"
	];
	return new App\Mail\AppMail($mail_infos);
});

Route::group(['prefix' => 'debug'], function() {
	Route::get('/', 'DebugController@index')->name('debug_index');
});
