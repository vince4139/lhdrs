<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'AuthController@login');
Route::post('/register', 'AuthController@register');
Route::post('/activate', 'AuthController@activate');

Route::middleware('auth:api')->group(function () {
	Route::get('/user', 'UserController@user');
	Route::get('/admin-menu', 'DashboardController@adminMenu');
	Route::get('/{player_id}/match-history', 'ScheduleController@history');
	Route::get('/{player_id}/nextFive', 'ScheduleController@nextFive');
	Route::get('/{player_id}/last-five', 'ScheduleController@lastFive');

	// pages related routes
	Route::get('/pages', 'PagesController@list');
	// Route::get('/page/{id}', 'PagesController@view');
	Route::put('/page/{id}/update', 'PagesController@update');
	Route::put('/page/{id}/publish', 'PagesController@publish');
	Route::put('/page/{id}/unpublish', 'PagesController@unpublish');
	Route::post('/page/create', 'PagesController@create');
	Route::delete('/page/{id}/delete', 'PagesController@destroy');

	// posts related routes
	Route::get('/posts', 'PostsController@list');
	// Route::get('/post/{id}', 'PostsController@view');
	Route::put('/post/{id}/update', 'PostsController@update');
	Route::put('/post/{id}/publish', 'PostsController@publish');
	Route::put('/post/{id}/unpublish', 'PostsController@unpublish');
	Route::post('/post/create', 'PostsController@create');
	Route::delete('/post/{id}/delete', 'PostsController@destroy');

	// Menus related routes
	Route::get('/menus', 'MenusController@list');
	Route::get('/menu/{id}/items', 'MenusController@menuItemsList');
	Route::delete('/menu/{id}/item/{menu_item_id}/delete', 'MenusController@menuItemDelete');
	Route::put('/menu/{id}/item/{menu_item_id}/update', 'MenusController@menuItemUpdate');
	Route::post('/menu/{id}/item/create', 'MenusController@menuItemCreate');
	// Route::get('/menu/{id}', 'MenusController@view');
	Route::put('/menu/{id}/update', 'MenusController@update');
	Route::post('/menu/create', 'MenusController@create');
	Route::delete('/menu/{id}/delete', 'MenusController@destroy');

	// Leagues related routes
	Route::get('/leagues', 'LeaguesController@list');
	Route::get('/league/{league_id}/seasons', 'SeasonsController@leagueSeasons');
	Route::put('/league/{id}/update', 'LeaguesController@update');
	Route::put('/league/{id}/order/increase', 'LeaguesController@modifyOrder');
	Route::put('/league/{id}/order/decrease', 'LeaguesController@modifyOrder');
	Route::post('/league/create', 'LeaguesController@create');
	Route::delete('/league/{id}/delete', 'LeaguesController@destroy');

	// Arenas related routes
	Route::get('/arenas', 'ArenasController@list');

	// Leagues Statuses
	Route::get('/leagues-statuses', 'LeagueStatusesController@list');

	// Leagues Types
	Route::get('/season-types', 'SeasonTypesController@list');

	// Match Types
	Route::get('/match-types', 'MatchTypesController@list');

	// Seasons related routes
	Route::get('/seasons', 'SeasonsController@list');
	Route::put('/season/{id}/update', 'SeasonsController@update');
	Route::put('/season/{id}/order/increase', 'SeasonsController@modifyOrder');
	Route::put('/season/{id}/order/decrease', 'SeasonsController@modifyOrder');
	Route::post('/season/create', 'SeasonsController@create');
	Route::delete('/season/{id}/delete', 'SeasonsController@destroy');
	
	// Games related routes
	Route::post('/season/{season_id}/game/create', 'ScheduleController@createSingleGame');
	Route::put('/season/{season_id}/game/{id}/update', 'ScheduleController@updateSingleGame');
	Route::post('/season/{id}/games/create', 'ScheduleController@generateSeasonGames');
	Route::get('/season/{id}/games', 'ScheduleController@getSeasonGames');
	Route::delete('/season/{season_id}/game/{game_id}/delete', 'ScheduleController@destroySeasonGame');

	// Players related routes
	Route::get('/players', 'PlayersController@list');
	Route::get('/season/{id}/players', 'PlayersController@getSeasonPlayers');
	
	
	


	Route::post('/logout', 'AuthController@logout'); 
});



Route::group(['prefix' => 'stats'], function() {
	Route::get('/players', 'StatsController@players');
	Route::get('/goalies', 'StatsController@goalies');
	Route::get('/player/{id}', 'StatsController@player');
	Route::get('/goalie/{id}', 'StatsController@goalie');
	Route::get('/last_five', 'StatsController@lastFive');
});

Route::group(['prefix' => 'schedule'], function() {
	Route::get('/player/{id}', 'ScheduleController@player');
	Route::get('/player/{id}/nextFive', 'ScheduleController@nextFive');
});

Route::group(['prefix' => 'news'], function() {
	// Route::get('/latest', 'NewsController@latest');
	// Route::get('/featured', 'NewsController@featured');
});
